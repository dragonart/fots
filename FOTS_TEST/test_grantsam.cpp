#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace FOTS_TEST
{		
	TEST_CLASS(GRANTSAMTABLE)
	{
		public:
			GRANTSAMTABLE()
			{
				table = ::DATABASE::GRANTSAMTABLE("grantsam.txt", &::DATABASE::DATABASE());
			}

			TEST_METHOD(LoadGrantsamTable)
			{
				Assert::IsTrue(table.Load());
			}
		
			TEST_METHOD(GrantsamGetData)
			{
				table.Load();
				
				::DATABASE::DATABASE_AMINOACIDS acidsData = ::DATABASE::DATABASE_AMINOACIDS("acids.txt", &::DATABASE::DATABASE());
				acidsData.Load();
				int index1 = acidsData.GetAcidFromLetter('N').GetIndex();
				int index2 = acidsData.GetAcidFromLetter('S').GetIndex();
				//Assert::AreEqual(4, index2);
				Assert::AreEqual(0.77, table.GetSimilarity(index1, index2));
			}

		private:
			::DATABASE::GRANTSAMTABLE table;
	};
}