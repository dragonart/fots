#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace FOTS_TEST
{		
	TEST_CLASS(FOTS_CORE)
	{
		public:
			FOTS_CORE()
			{

			}
		
			TEST_METHOD(DoubleToString)
			{
				double number = 0.23;

				std::string result = "0.23";

				Assert::AreEqual(result, ::DoubleToString(number));
			}
	};
}