#include "fots.h"


#define WORK_NUCLEOTIDES	1
#define WORK_CODONS			2
#define WORK_AMINOACIDS		3
#define WORK_GROUPS			4
#define WORK_PEPTIDES		5
#define WORK_DNAS			6
#define WORK_RNAS			7
#define WORK_CLASSES		8
#define WORK_GRANTSAM		9

#define MENU_RETURN	255

#define NUCLEOTIDES_SHOWALL			11
#define NUCLEOTIDES_SHOW			12
#define NUCLEOTIDES_MUTATION		13

#define CODONS_SHOWALL				21
#define CODONS_SHOW					22	
#define CODONS_MUTATION				23

#define AMINOACIDS_SHOWALL			31
#define AMINOACIDS_SHOW				32
#define AMINOACIDS_MUTATION			33

#define GROUPS_SHOWALL				41
#define GROUPS_SHOW					42

#define PEPTIDES_SHOWALL			51
#define PEPTIDES_SHOW				52
#define PEPTIDES_MUTATION			53
#define PEPTIDES_ADDFROMKEYBOARD	54
#define PEPTIDES_ADDFROMFILE		55

#define DNAS_SHOWALL				61
#define DNAS_SHOW					62
#define DNAS_MUTATION				63
#define DNAS_ADDFROMKEYBOARD		64
#define DNAS_ADDFROMFILE			65


short Menu(DATABASE& database)
{
	short ans;
	
	do
	{
		std::system("cls");
		std::cout << "Choose the section you want to work with:\n";
		std::cout << "1. Nucleotides\n";
		std::cout << "2. Codons\n";
		std::cout << "3. Aminoacids\n";
		std::cout << "4. Groups of aminoacids\n";
		std::cout << "5. Peptides\n";
		std::cout << "6. DNAs\n";
		std::cout << "7. RNAs\n";
		std::cout << "8. Classes of peptides\n";
		std::cout << "9. Grantsam' table\n\n";
		std::cout << "0. Exit\n\n";

		std::cout << "Your choice: ";

		std::cin >> ans;


		switch (ans)
		{
			case WORK_NUCLEOTIDES:		
				std::system("cls");
				std::cout << "DATABASE NUCLEOTIDES:\n";
				std::cout << "1. Show all data\n\n";
				std::cout << "2. Show information about nucleotide\n\n";
				std::cout << "3. Determine nucleotide mutation\n\n";
				std::cout << "0. Return to sections\n\n";
			
				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_NUCLEOTIDES * 10;
				else
					ans = MENU_RETURN;

				break;

			case WORK_CODONS:
				std::system("cls");
				std::cout << "DATABASE CODONS:\n";
				std::cout << "1. Show all data\n\n";
				std::cout << "2. Show information about codon\n\n";
				std::cout << "3. Determine codon mutation\n\n";
				std::cout << "0. Return to sections\n\n";

				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_CODONS * 10;
				else
					ans = MENU_RETURN;

				break;

			case WORK_AMINOACIDS:
				std::system("cls");
				std::cout << "DATABASE AMINOACIDS:\n";
				std::cout << "1. Show all data\n\n";
				std::cout << "2. Show information about acid\n\n";
				std::cout << "3. Determine acid mutation\n\n";
				std::cout << "0. Return to sections\n\n";

				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_AMINOACIDS * 10;
				else
					ans = MENU_RETURN;
				
				break;

			case WORK_GROUPS:
				std::system("cls");
				std::cout << "DATABASE GROUPS OF AMINOACIDS:\n";
				std::cout << "1. Show all data\n";
				std::cout << "2. Show information about group\n\n";
				std::cout << "0. Return to sections\n\n";

				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_GROUPS * 10;
				else
					ans = MENU_RETURN;

				break;

			case WORK_PEPTIDES:
				std::system("cls");
				std::cout << "DATABASE PEPTIDES [" << database.GetPeptidesData().GetSize() << "]:\n";
				std::cout << "1. Show all data\n\n";
				std::cout << "2. Show information about peptide\n\n";
				std::cout << "3. Determine peptide mutation\n\n";
				std::cout << "4. Add peptides (from keyboard)\n";
				std::cout << "5. Add peptides (from file)\n\n";
				std::cout << "0. Return to sections\n\n";

				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_PEPTIDES * 10;
				else
					ans = MENU_RETURN;

				break;

			case WORK_DNAS:
				std::system("cls");
				std::cout << "DATABASE DNAS [" << database.GetDNAsData().GetSize() << "]:\n";
				std::cout << "1. Show all data\n\n";
				std::cout << "2. Show information about DNA\n\n";
				std::cout << "3. Determine DNA mutation\n\n";
				std::cout << "4. Add DNAs (from keyboard)\n";
				std::cout << "5. Add DNAs (from file)\n\n";
				std::cout << "0. Return to sections\n\n";

				std::cout << "Your choice: ";
				std::cin >> ans;
				if(ans)
					ans += WORK_DNAS * 10;
				else
					ans = MENU_RETURN;

				break;

			case WORK_RNAS:
				break;

			case WORK_CLASSES:
				break;

			case WORK_GRANTSAM:
				break;

			default:
				break;
		}
	}
	while(ans == MENU_RETURN);

	return ans;
}
	
// ����� �����
int main(int argc, char** argv)
{
	DATABASE database("nucleotides.txt", "codons.txt", "acids.txt", "groups.txt", "peptides.txt", "dnas.txt", "rnas.txt", "classes.txt", "grantsam.txt");
	bool loaded = database.LoadAll();

	
	if(loaded)
		std::cout << "DATABASE is successfully loaded.\n";
	else
		std::cout << "DATABASE failed to load.\n";


	if(argc > 1)
	{
		for(int i = 1; i < argc; i++)
			if(database.GetPeptidesData().AddFromFile(argv[i], DATABASE_ADDNEW))
				std::cout << argv[i] << " is successfully added.\n";
			else
				std::cout << "Failed to add " << argv[i] << ".\n";

		system("pause");
	}


	short menu;
	std::string fileName;
	LONG index, index1, index2;
	char letter, letter1, letter2;
	char line[PEPTIDE_MAX_LENGTH];
	MUTATION_PEPTIDE mutationPeptide;
	MUTATION_DNA mutationDNA;
	std::vector <std::string> lines;

	while(menu = Menu(database))
	{
		std::system("cls");

		switch(menu)
		{
			case NUCLEOTIDES_SHOWALL:
				database.GetNucleotidesData().ShowData();
				break;

			case NUCLEOTIDES_SHOW:
				std::cout << "Enter letter of nucleotide you want to know about: ";
				std::cin >> letter;

				std::cout << "\n";
				database.GetNucleotidesData().ShowNucleotideFromLetter(letter);
				std::cout << "\n";

				break;

			case NUCLEOTIDES_MUTATION:		
				std::cout << "Enter nucleotide #1 letter: ";
				std::cin >> letter1;
				std::cout << "Enter nucleotide #2 letter: ";
				std::cin >> letter2;

				index1 = (SHORT)database.GetNucleotidesData().GetNucleotideFromLetter(letter1);
				index2 = (SHORT)database.GetNucleotidesData().GetNucleotideFromLetter(letter2);

				std::cout << "\n";
				database.GetNucleotidesData().DetermineNucleotideMutation(index1, index2, 0).Show();
				std::cout << "\n";
				
				break;


			case CODONS_SHOWALL:
				database.GetCodonsData().ShowData();
				break;

			case CODONS_SHOW:
				std::cout << "Enter letters of codon you want to know about: ";
				std::cin.ignore(1);
				std::cin.getline(line, 5);

				std::cout << "\n";
				database.GetCodonsData().ShowCodonFrom3Letters(line[0], line[1], line[2]);
				std::cout << "\n";

				break;

			case CODONS_MUTATION:
				std::cout << "Enter codon #1: ";
				std::cin.ignore(1);
				std::cin.getline(line, 5);
				index1 = (SHORT)database.GetCodonsData().GetCodonFrom3Letters(line[0], line[1], line[2]);
				std::cout << "Enter codon #2: ";
				std::cin.getline(line, 5);
				index2 = (SHORT)database.GetCodonsData().GetCodonFrom3Letters(line[0], line[1], line[2]);
				
				std::cout << "\n";
				database.GetCodonsData().DetermineCodonMutation(index1, index2, 0).Show();
				std::cout << "\n";

				break;


			case AMINOACIDS_SHOWALL:
				database.GetAcidsData().ShowData();
				break;

			case AMINOACIDS_SHOW:
				std::cout << "Enter letter of aminoacid you want to know about: ";
				std::cin >> letter;

				std::cout << "\n";
				database.GetAcidsData().ShowAcidFromLetter(letter);
				std::cout << "\n";
				
				break;

			case AMINOACIDS_MUTATION:
				std::cout << "Enter aminoacid #1 letter: ";
				std::cin >> letter1;
				std::cout << "Enter aminoacid #2 letter: ";
				std::cin >> letter2;

				index1 = (SHORT)database.GetAcidsData().GetAcidFromLetter(letter1);
				index2 = (SHORT)database.GetAcidsData().GetAcidFromLetter(letter2);

				std::cout << "\n";
				database.GetAcidsData().DetermineAcidMutation(index1, index2, 0).Show();
				std::cout << "\n";
				
				break;


			case GROUPS_SHOWALL:
				database.GetGroupsData().ShowData();
				break;

			case GROUPS_SHOW:
				std::cout << "Enter index of group you want to know about: ";
				std::cin >> index;

				std::cout << "\n";
				database.GetGroupsData().ShowGroupFromIndex(index);
				std::cout << "\n";
				
				break;


			case PEPTIDES_SHOWALL:
				database.GetPeptidesData().ShowData();
				break;

			case PEPTIDES_SHOW:
				std::cout << "Enter full or part of name of peptide you want to know about: ";
				std::cin.ignore(1);
				std::cin.getline(line, 100);

				std::cout << "\n";
				index = (LONG)database.GetPeptidesData().SearchPeptidesAndChooseOne(line);
				if(index)
				{
					std::system("cls");
					database.GetPeptidesData().ShowPeptideFromIndex(index);
				}
				std::cout << "\n";

				break;

			case PEPTIDES_MUTATION:
				std::cout << "Enter full or part of name of peptide #1: ";
				std::cin.ignore(1);
				std::cin.getline(line, 100);
				index1 = (LONG)database.GetPeptidesData().SearchPeptidesAndChooseOne(line);

				std::cout << "Enter full or part of name of peptide #2: ";
				std::cin.getline(line, 100);
				index2 = (LONG)database.GetPeptidesData().SearchPeptidesAndChooseOne(line);

				
				mutationPeptide = database.GetPeptidesData().DeterminePeptideMutation(index1, index2);
				if(mutationPeptide == database.GetPeptidesData().GetFakeMutation())
					std::cout << "\nERROR: such mutation can't be determined.\n";
				else
				{
					std::cout << "\nWhat to do with mutation:\n";
					std::cout << "1. Show\n";
					std::cout << "2. Save to file\n";
					std::cout << "3. Both\n";
					std::cout << "Your choice: ";
					std::cin >> index;


					std::cout << "\n";
					switch(index)
					{
						case 1:
							std::system("cls");
							mutationPeptide.Show();
							break;

						case 2:
							std::cout << "Enter file name: ";
							std::cin.ignore(1);
							std::getline(std::cin, fileName);

							std::cout << "\n";
							mutationPeptide.SaveToFile(fileName);

							break;

						case 3:
							std::cout << "Enter file name: ";
							std::cin.ignore(1);
							std::getline(std::cin, fileName);

							std::system("cls");
							mutationPeptide.Show();
							mutationPeptide.SaveToFile(fileName);

							break;
				
						default:
							break;
					}
				}

				break;
				
			case PEPTIDES_ADDFROMKEYBOARD:
				index = 1;
				do
				{
					if(database.GetPeptidesData().AddFromKeyboard())
						std::cout << "Add new peptide - success!\n";
					else
						std::cout << "Add new peptide - FAIL!\n";
					
					std::cout << "\nEnter 1 to add another peptide or 0 to return to menu: ";
					std::cin >> index;
				}
				while(index);

				break;

			case PEPTIDES_ADDFROMFILE:
				std::cout << "Enter file name: ";
				std::cin.ignore(1);
				std::getline(std::cin, fileName);

				std::cout << "\n";
				if(database.GetPeptidesData().AddFromFile(fileName, DATABASE_ADDNEW))
					database.GetPeptidesData().Save();
				else
					std::cout << "Failed to add peptides from file " << fileName << "\n";
				std::cout << "\n";

				break;


			case DNAS_SHOWALL:
				database.GetDNAsData().ShowData();
				break;

			case DNAS_SHOW:
				std::cout << "Enter full or part of name of DNA you want to know about: ";
				std::cin.ignore(1);
				std::cin.getline(line, 100);

				std::cout << "\n";
				index = (LONG)database.GetDNAsData().SearchDNAsAndChooseOne(line);
				if(index)
				{
					std::system("cls");
					database.GetDNAsData().ShowDNAFromIndex(index);
				}
				std::cout << "\n";

				break;

			case DNAS_MUTATION:
				std::cout << "Enter full or part of name of DNA #1: ";
				std::cin.ignore(1);
				std::cin.getline(line, 100);
				index1 = (LONG)database.GetDNAsData().SearchDNAsAndChooseOne(line);

				std::cout << "Enter full or part of name of DNA #2: ";
				std::cin.getline(line, 100);
				index2 = (LONG)database.GetDNAsData().SearchDNAsAndChooseOne(line);
				
				mutationDNA = database.GetDNAsData().DetermineDNAMutation(index1, index2);
				if(mutationDNA == database.GetDNAsData().GetFakeMutation())
					std::cout << "\nERROR: such mutation can't be determined.\n";
				else
				{
					std::cout << "\nWhat to do with mutation:\n";
					std::cout << "1. Show\n";
					std::cout << "2. Save to file\n";
					std::cout << "3. Both\n";
					std::cout << "Your choice: ";
					std::cin >> index;

					std::cout << "\n";
					switch(index)
					{
						case 1:
							std::system("cls");
							mutationDNA.Show();
							break;

						case 2:
							std::cout << "Enter file name: ";
							std::cin.ignore(1);
							std::getline(std::cin, fileName);

							std::cout << "\n";
							mutationDNA.SaveToFile(fileName);

							break;

						case 3:
							std::cout << "Enter file name: ";
							std::cin.ignore(1);
							std::getline(std::cin, fileName);

							std::system("cls");
							mutationDNA.Show();
							mutationDNA.SaveToFile(fileName);

							break;
				
						default:
							break;
					}
				}

				break;

			case DNAS_ADDFROMKEYBOARD:
				index = 1;
				do
				{
					if(database.GetDNAsData().AddFromKeyboard())
						std::cout << "Add new DNA - success!\n";
					else
						std::cout << "Add new DNA - FAIL!\n";
					
					std::cout << "\nEnter 1 to add another dna or 0 to return to menu: ";
					std::cin >> index;
				}
				while(index);

				break;

			case DNAS_ADDFROMFILE:
				std::cout << "Enter file name: ";
				std::cin.ignore(1);
				std::getline(std::cin, fileName);

				std::cout << "\n";
				if(database.GetDNAsData().AddFromFile(fileName, DATABASE_ADDNEW))
					database.GetDNAsData().Save();
				else
					std::cout << "Failed to add DNAs from file " << fileName << "\n";
				std::cout << "\n";

				break;


			case 7:
				database.GetRNAsData().ShowData();
				break;

			case 8:
				database.GetPeptideClassesData().ShowData();
				break;

			case 9:
				database.GetGrantsamTable().ShowData();
				break;

			default:
				break;
		}

		std::system("pause");
	}
	
	
	database.SaveAll();
	return 0;
}