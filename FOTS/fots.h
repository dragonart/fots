#include "../FOTS_CORE/database.h"

#include "../FOTS_CORE/nucleotide.h"
#include "../FOTS_CORE/codon.h"
#include "../FOTS_CORE/aminoacid.h"
#include "../FOTS_CORE/peptide.h"
#include "../FOTS_CORE/dna.h"
#include "../FOTS_CORE/rna.h"
#include "../FOTS_CORE/peptideclass.h"

#include "../FOTS_CORE/mutation_nucleotide.h"
#include "../FOTS_CORE/mutation_codon.h"
#include "../FOTS_CORE/mutation_aminoacid.h"
#include "../FOTS_CORE/mutation_peptide.h"
#include "../FOTS_CORE/mutation_dna.h"

#include "../FOTS_CORE/fots_core.h"


#include <iostream>
#include <string>