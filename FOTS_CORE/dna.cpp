#include "dna.h"
#include "database.h"
#include "nucleotide.h"


DNA::DNA()
{
	index = 0;
	rnaIndex = 0;
	peptideIndex = 0;
	peptideClassIndex = 0;

	name = "empty DNA";
	line = "";

	database = NULL;
}

DNA::DNA(
		const DNA& other)
{
	index = other.index;
	rnaIndex = other.rnaIndex;
	peptideIndex = other.peptideIndex;
	peptideClassIndex = other.peptideClassIndex;


	name = other.name;
	line = other.line;

	nucleotidesIndices = other.nucleotidesIndices;

	database = other.database;
}

DNA::DNA(
		const LONG newIndex, 
		const LONG newRNAIndex,
		const LONG newPeptideIndex,
		const LONG newPeptideClassIndex,
		const std::string newName, 
		const std::string newLine,
		DATABASE* newDatabase)
{
	database = newDatabase;

	index = newIndex;
	rnaIndex = newRNAIndex;
	peptideIndex = newPeptideIndex;
	peptideClassIndex = newPeptideClassIndex;

	name = std::string(newName);
	line = std::string(newLine);

	// ��������� ������� �������� �����������
	for(unsigned int i = 0; i < line.length(); i++)
		nucleotidesIndices.push_back((SHORT)database->nucleotidesData.GetNucleotideFromLetter(line[i]));
}



std::string DNA::GetName()
{
	return name;
}


NUCLEOTIDE& DNA::operator[](const unsigned int position)
{
	return GetNucleotideAtPosition(position);
}

NUCLEOTIDE& DNA::GetNucleotideAtPosition(const unsigned int position)
{
	return database->nucleotidesData[nucleotidesIndices[position]];
}


SHORT DNA::GetNucleotideIndexAtPosition(const unsigned int position)
{
	return nucleotidesIndices[position];
}


RNA& DNA::GetRNA()
{
	return database->rnasData[rnaIndex];
}

LONG DNA::GetRNAIndex()
{
	return  rnaIndex;
}


double DNA::CalculateBeloserskyCoefficient()
{
	/*		� * �
		K =	-----
			� + �
					*/
	double result;
	double gNumber = 0.0, cNumber = 0.0, aNumber = 0.0, tNumber = 0.0;


	for(unsigned int i = 0; i < nucleotidesIndices.size(); i++)
		switch((SHORT)database->nucleotidesData[nucleotidesIndices[i]])
		{
			case NUCLEOTIDE_GUANINE:
				gNumber++;
				break;

			case NUCLEOTIDE_CYTOSINE:
				cNumber++;
				break;

			case NUCLEOTIDE_ADENINE:
				aNumber++;
				break;

			case NUCLEOTIDE_TIMINE:
				tNumber++;
				break;

			default:
				break;
		}

	result = gNumber * cNumber / (aNumber + tNumber);


	return result;
}
		

unsigned int DNA::GetLength()
{
	return nucleotidesIndices.size();
}


PEPTIDE& DNA::GetPeptide()
{
	return database->peptidesData[peptideIndex];
}

LONG DNA::GetPeptideIndex()
{
	return peptideIndex;
}


PEPTIDECLASS& DNA::GetPeptideClass()
{
	return database->peptideClassesData[peptideClassIndex];
}

LONG DNA::GetPeptideClassIndex()
{
	return peptideClassIndex;
}



DNA::~DNA()
{
}