#pragma once
#include "fots_core.h"
#include "database.h"


class MUTATION_DNA
{
	public:
		MUTATION_DNA();
		MUTATION_DNA(
				const MUTATION_DNA& other);
		MUTATION_DNA(
				const LONG dna1Index,
				const LONG dna2Index,
				DATABASE* database);
		MUTATION_DNA& operator=(
				const MUTATION_DNA& other);


		std::string GetProcess();

		double GetDifficulty();

		int GetTransitionsNumber();
		int GetTransversionsNumber();

		int GetTransversionsFromPurineToPyrimidineNumber();
		int GetTransversionsFromPyrimidineToPurineNumber();

		int GetMutationsNumber();

		void Show();
		void SaveToFile(
				const std::string fileName);

		double CalculateEqualNucleotidesPart();
		
		bool operator==(const MUTATION_DNA& other);


		~MUTATION_DNA();


	private:
		LONG dna1Index;
		LONG dna2Index;

		std::string process;

		double difficulty;

		int transitionsNumber;
		int transversionsNumber;

		int transversionsFromPurineToPyrimidineNumber;

		std::string line1;
		std::string line2;

		LONG length;

		std::vector <MUTATION_NUCLEOTIDE> mutations;

		DATABASE* database;
};

