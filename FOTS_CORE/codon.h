#pragma once
#include "fots_core.h"
#include "database.h"


class CODON
{
	public:
		CODON();
		CODON(
				const CODON& other);
		CODON(
				const SHORT index,
				const SHORT acidIndex,
				std::vector <SHORT> nucleotidesIndices,
				DATABASE* database);


		SHORT GetAcidIndex();
		AMINOACID& GetAcid();

		NUCLEOTIDE& operator[](
				const SHORT positionIndex);
		NUCLEOTIDE& GetNucleotideAtPosition(
				const SHORT positionIndex);

		SHORT GetNucleotideIndexAtPosition(
				const SHORT positionIndex);

		operator SHORT() const {return index;}
		operator std::string() {return CodonToString();}


		~CODON();


	private:
		SHORT index;
		SHORT acidIndex;

		std::vector <SHORT> nucleotidesIndices;

		DATABASE* database;

		
		std::string CodonToString();
};