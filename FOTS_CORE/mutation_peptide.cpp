#include "mutation_peptide.h"
#include "mutation_aminoacid.h"
#include "mutation_codon.h"
#include "database.h"
#include "peptide.h"
#include "aminoacid.h"


MUTATION_PEPTIDE::MUTATION_PEPTIDE()
{
	peptide1Index = 0;
	peptide2Index = 0;

	process = std::string("");

	difficulty = 0.0;
	similarity = 0.0;

	transitionsNumber = 0;
	transversionsNumber = 0;
	
	groupChangesNumber = 0;

	spacesNumber = 0;

	line1 = "";
	line2 = "";

	length = 0;

	database = NULL;
}

MUTATION_PEPTIDE::MUTATION_PEPTIDE(
		const MUTATION_PEPTIDE& other)
{
	peptide1Index = other.peptide1Index;
	peptide2Index = other.peptide2Index;

	process = other.process;

	difficulty = other.difficulty;
	similarity = other.similarity;

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	groupChangesNumber = other.groupChangesNumber;

	acidsMutations = other.acidsMutations;

	spacesNumber = other.spacesNumber;

	line1 = other.line1;
	line2 = other.line2;

	length = other.length;

	database = other.database;
}

MUTATION_PEPTIDE::MUTATION_PEPTIDE(
		const LONG newPeptide1Index,
		const LONG newPeptide2Index,
		DATABASE* newDatabase)
{
	database = newDatabase;

	peptide1Index = newPeptide1Index;
	peptide2Index = newPeptide2Index;

	line1 = (std::string)database->peptidesData[peptide1Index];
	line2 = (std::string)database->peptidesData[peptide2Index];

	/*
		������������ �����
	*/
	std::vector <std::string> lines = database->AlignLines(ALIGN_PEPTIDES, peptide1Index, peptide2Index);

	line1 = lines[0];
	line2 = lines[1];
	
	length = line1.length();
	

	// ��������� ���� ����������
	process = std::string("");

	difficulty = 0.0;
	similarity = 0.0;

	transitionsNumber = transversionsNumber = groupChangesNumber = spacesNumber = 0;

	short gapStarted = 0; // 0 - ������� ���. 1 - � ������, 2 - �� ������


	for(LONG i = 0; i < length; i++)
	{
		SHORT index1 = (SHORT)database->acidsData.GetAcidFromLetter(line1[i]);
		SHORT index2 = (SHORT)database->acidsData.GetAcidFromLetter(line2[i]);


		// ������� ����������� ������������� �������� ��������
		if(index1 == index2)
		{
			similarity += 1.0;
			continue;
		}


		// ���� ���� �� ����������� �����, ������ ����� ����� ������
		if(!(index1 * index2))
		{
			spacesNumber++;

			// ���� ������ ������ ��� ��������, ����� ���� ��������
			if(!gapStarted)
			{
				process += "--------------[ gap in peptide #";

				if(!index1)
				{
					gapStarted = 1;
					process += "1 ]---------------\n";
				}
				else
				{
					gapStarted = 2;
					process += "2 ]---------------\n";
				}
			}
			else
				// �������� ����, ���� ����� ������ �� � ��� �� �������
				if((gapStarted == 1) && !index2)
				{
					gapStarted = 2;
					process += "--------------[ gap in peptide #2 ]---------------\n";
				}
				else
					if((gapStarted == 2) && !index1)
					{
						gapStarted = 1;
						process += "--------------[ gap in peptide #1 ]---------------\n";
					}

			continue;
		}
		

		gapStarted = 0;
		acidsMutations.push_back(MUTATION_AMINOACID(index1, index2, i + 1, length, database));


		/*
			������ ������:
			� ������� ��� ������� �����������
												*/
		process += acidsMutations[acidsMutations.size() - 1].GetProcess() + "\n";

		difficulty += acidsMutations[acidsMutations.size() - 1].GetBestMutation().GetDifficulty();
		similarity += database->grantsamTable.GetSimilarity(index1, index2);

		transitionsNumber += acidsMutations[acidsMutations.size() - 1].GetBestMutation().GetTransitionsNumber();
		transversionsNumber += acidsMutations[acidsMutations.size() - 1].GetBestMutation().GetTransversionsNumber();

		groupChangesNumber += acidsMutations[acidsMutations.size() - 1].GetBestMutation().MutationChangesAcidGroup();
	}

	similarity /= length;
}


MUTATION_PEPTIDE& MUTATION_PEPTIDE::operator=(
		const MUTATION_PEPTIDE& other)
{
	peptide1Index = other.peptide1Index;
	peptide2Index = other.peptide2Index;

	process = other.process;

	difficulty = other.difficulty;
	similarity = other.similarity;

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	groupChangesNumber = other.groupChangesNumber;

	acidsMutations = other.acidsMutations;

	spacesNumber = other.spacesNumber;

	line1 = other.line1;
	line2 = other.line2;

	length = other.length;

	database = other.database;

	return *this;
}



std::string MUTATION_PEPTIDE::GetProcess()
{
	return process;
}


double MUTATION_PEPTIDE::GetDifficulty()
{
	return difficulty;
}


unsigned int MUTATION_PEPTIDE::GetTransitionsNumber()
{
	return transitionsNumber;
}

unsigned int MUTATION_PEPTIDE::GetTransversionsNumber()
{
	return transversionsNumber;
}


unsigned int MUTATION_PEPTIDE::GetAcidMutationsNumber()
{
	return acidsMutations.size();
}

unsigned int MUTATION_PEPTIDE::GetGroupChangesNumber()
{
	return groupChangesNumber;
}


void MUTATION_PEPTIDE::Show()
{
	std::cout << "PEPTIDE #1: " << database->peptidesData[peptide1Index].GetName() << "\n";
	std::cout << "PEPTIDE #2: " << database->peptidesData[peptide2Index].GetName() << "\n";
	std::cout << "LINE #1: " << line1 << "\n";
	std::cout << "LINE #2: " << line2 << "\n";
	if(peptide1Index == peptide2Index)
		std::cout << "THERE IS NO MUTATION\n";
	else
	{
		std::cout << "PROCESS:\n\n" << process << "\n\n";
		std::cout << "SIMILARITY (GRANTSAM): " << CalculateSimilarityGrantsam() * 100.0 << "%\n"; 
		std::cout << "PART OF EQULAL AMINOACIDS: " << CalculateEqualAcidsPart() * 100.0 << "%\n";
		std::cout << "TOTAL ACID CHANGES: " << acidsMutations.size() << "\n";
		std::cout << "TOTAL TRANSITIONS: " << transitionsNumber << "\n";
		std::cout << "TOTAL TRANSVERSIONS: " << transversionsNumber << "\n";
		std::cout << "TOTAL GROUP CHANGES: " << groupChangesNumber << "\n";
	}
}

void MUTATION_PEPTIDE::SaveToFile(
		const std::string fileName)
{
	std::ofstream file;

	file.open(fileName);
	if(!file.is_open())
	{
		file.close();
		
		file.open(fileName, std::ios_base::out | std::ios_base::trunc);
		if(!file.is_open())
		{
			std::cout << "\nCan't save to file '" << fileName << "'.\n";
			return;
		}
	}
		
	
	file << "PEPTIDE #1: " << database->peptidesData[peptide1Index].GetName() << "\n";
	file << "PEPTIDE #2: " << database->peptidesData[peptide2Index].GetName() << "\n";
	file << "LINE #1: " << line1 << "\n";
	file << "LINE #2: " << line2 << "\n";
	if(peptide1Index == peptide2Index)
		file << "THERE IS NO MUTATION\n";
	else
	{
		file << "PROCESS:\n\n" << process << "\n\n";
		file << "SIMILARITY: " << CalculateSimilarityGrantsam() * 100.0 << "%\n"; 
		file << "PART OF EQUAL AMINOACIDS: " << CalculateEqualAcidsPart() * 100.0 << "%\n";
		file << "TOTAL ACID CHANGES: " << acidsMutations.size() << "\n";
		file << "TOTAL TRANSITIONS: " << transitionsNumber << "\n";
		file << "TOTAL TRANSVERSIONS: " << transversionsNumber << "\n";
		file << "TOTAL GROUP CHANGES: " << groupChangesNumber << "\n";
	}

	file.close();
	std::cout << "Mutation successfully saved to file.\n";
}


double MUTATION_PEPTIDE::CalculateEqualAcidsPart()
{
	return (double)(length - acidsMutations.size() - spacesNumber) / (double)length;
}

double MUTATION_PEPTIDE::CalculateSimilarityGrantsam()
{
	return similarity;
}


bool MUTATION_PEPTIDE::operator==(
		const MUTATION_PEPTIDE& other)
{
	if(peptide1Index != other.peptide1Index)
		return false;
	if(peptide2Index != other.peptide2Index)
		return false;

	return true;
}



MUTATION_PEPTIDE::~MUTATION_PEPTIDE()
{
}
