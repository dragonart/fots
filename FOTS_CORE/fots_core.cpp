#include "fots_core.h"


std::string EnterSpacesForGoodNumberReading(
		LONG number, 
		LONG max = 999)
{
	std::string result("");

	while(number && max)
	{
		number /= 10;
		max /= 10;
	}

	while(max)
	{
		max /= 10;
		result += ' ';
	}

	return result;
	return "";
}


bool LinesAreSimilar(
		const std::string line1, 
		const std::string line2)
{
	LONG length = line1.length();
	if(line2.length() < length)
		length = line2.length();


	for(LONG i = 0; i < length; i++)
		if(line1[i] != line2[i])
			return false;


	return true;
}


bool LinesAreEqual(
		const std::string line1,
		const std::string line2)
{
	if(line1.length() != line2.length())
		return false;

	int length = line1.length();
	for(int i = 0; i < length; i++)
		if(line1[i] != line2[i])
			return false;

	return true;
}


std::string NumberToString(
		LONG number)
{
	if(!number)
		return std::string("0");


	std::string result("");
	LONG x = number;

	// ����� ���� � ������
	while(x)
	{
		result += (x % 10) + CHAR_ZERO;
		x /= 10;
	}

	// �������������� ������: "137" -> "731"
	for(LONG i = 0; i < result.length() / 2; i++)
	{
		char temp = result[i];
		result[i] = result[result.length() - i - 1];
		result[result.length() - i - 1] = temp;
	}

	return result;
}


std::string DoubleToString(
		const double number)
{
	std::ostringstream strs;
	
	strs << number;
	std::string result = strs.str();

	return result;
}


double Max(
		const double x,
		const double y,
		const double z)
{
	double max = (x > y) ? x : y;
	max = (z > max) ? z : max;

	return max;
}