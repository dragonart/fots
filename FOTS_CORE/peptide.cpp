#include "peptide.h"
#include "database.h"
#include "aminoacid.h"


PEPTIDE::PEPTIDE()
{
	index = 0;
	dnaIndex = 0;

	name = "";
	line = "";

	database = NULL;
}

PEPTIDE::PEPTIDE(
		const PEPTIDE& other)
{
	index = other.index;
	dnaIndex = other.dnaIndex;

	name = other.name;
	line = other.line;

	acidsIndices = other.acidsIndices;

	database = other.database;
}

PEPTIDE::PEPTIDE(
		const LONG newIndex,
		const LONG newDNAIndex,
		const std::string newName, 
		const std::string newLine,
		DATABASE* newDatabase)
{
	database = newDatabase;

	index = newIndex;
	dnaIndex = newDNAIndex;
	
	name = std::string(newName);
	line = std::string(newLine);
	

	// ��������� ������� �������� �����������, �������� � ������
	for(unsigned int i = 0; i < line.size(); i++)
	{
		acidsIndices.push_back((SHORT)database->acidsData.GetAcidFromLetter(line[i]));
	}
}



LONG PEPTIDE::GetDNAIndex()
{
	return dnaIndex;
}

DNA& PEPTIDE::GetDNA()
{
	return database->dnasData[dnaIndex];
}


std::string PEPTIDE::GetName()
{
	return name;
}


AMINOACID& PEPTIDE::operator[](
		const unsigned int positionInPeptide)
{
	return GetAcid(positionInPeptide);
}

AMINOACID& PEPTIDE::GetAcid(
		const unsigned int positionInPeptide)
{
	return database->acidsData[acidsIndices[positionInPeptide]];
}


SHORT PEPTIDE::GetAcidIndex(
		const unsigned int positionInPeptide)
{
	return acidsIndices[positionInPeptide];
}


unsigned int PEPTIDE::GetAcidNumber(
		const SHORT acidIndex)
{
	unsigned int result = 0;

	for(unsigned int i = 0; i < acidsIndices.size(); i++)
		if(acidsIndices[i] == acidIndex)
			result++;

	return result;
}

double PEPTIDE::GetAcidStake(
		const SHORT acidIndex)
{
	unsigned int number = GetAcidNumber(acidIndex);

	double result = (double)number / (double)GetLength(); 

	return result;
}


LONG PEPTIDE::GetLength()
{
	return acidsIndices.size();
}



PEPTIDE::~PEPTIDE()
{
}
