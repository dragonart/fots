﻿#include "database.h"

#include "nucleotide.h"
#include "codon.h"
#include "aminoacid.h"
#include "aminoacidgroup.h"
#include "peptide.h"
#include "dna.h"
#include "rna.h"
#include "peptideclass.h"

#include "mutation_nucleotide.h"
#include "mutation_codon.h"
#include "mutation_aminoacid.h"
#include "mutation_peptide.h"
#include "mutation_dna.h"


DATABASE::DATABASE()
{
	nucleotidesData = DATABASE_NUCLEOTIDES();
	codonsData = DATABASE_CODONS();
	acidsData = DATABASE_AMINOACIDS();
	groupsData = DATABASE_AMINOACIDGROUPS();
	peptidesData = DATABASE_PEPTIDES();
	dnasData = DATABASE_DNAS();
	rnasData = DATABASE_RNAS();
	peptideClassesData = DATABASE_PEPTIDECLASSES();
	grantsamTable = GRANTSAMTABLE();
	aligner = LINEALIGNER();
}

DATABASE::DATABASE(
		const DATABASE& other)
{
	nucleotidesData = other.nucleotidesData;
	codonsData = other.codonsData;
	acidsData = other.acidsData;
	groupsData = other.groupsData;
	peptidesData = other.peptidesData;
	dnasData = other.dnasData;
	rnasData = other.rnasData;
	peptideClassesData = other.peptideClassesData;
	grantsamTable = other.grantsamTable;
	aligner = other.aligner;
}

DATABASE::DATABASE(
		const std::string fileNucleotides, 
		const std::string fileCodons, 
		const std::string fileAcids, 
		const std::string fileGroups, 
		const std::string filePeptides, 
		const std::string fileDNAs, 
		const std::string fileRNAs,
		const std::string filePeptideClasses,
		const std::string fileGrantsamTable)
{
	nucleotidesData = DATABASE_NUCLEOTIDES(fileNucleotides, this);
	codonsData = DATABASE_CODONS(fileCodons, this);
	acidsData = DATABASE_AMINOACIDS(fileAcids, this);
	groupsData = DATABASE_AMINOACIDGROUPS(fileGroups, this);
	peptidesData =  DATABASE_PEPTIDES(filePeptides, this);
	dnasData = DATABASE_DNAS(fileDNAs, this);
	rnasData = DATABASE_RNAS(fileRNAs, this);
	peptideClassesData = DATABASE_PEPTIDECLASSES(filePeptideClasses, this);
	grantsamTable = GRANTSAMTABLE(fileGrantsamTable, this);
	aligner = LINEALIGNER(this);
}


bool DATABASE::SaveAll()
{
	bool result = nucleotidesData.Save() && codonsData.Save() && acidsData.Save() && groupsData.Save() && peptidesData.Save() && dnasData.Save() && rnasData.Save() && peptideClassesData.Save() && grantsamTable.Save();
	
	return result;
}

bool DATABASE::LoadAll()
{
	bool result = nucleotidesData.Load() && codonsData.Load() && acidsData.Load() && groupsData.Load() && peptidesData.Load() && dnasData.Load() && rnasData.Load() && peptideClassesData.Load() && grantsamTable.Load();

	return result;
}


DATABASE::DATABASE_NUCLEOTIDES& DATABASE::GetNucleotidesData()
{
	return nucleotidesData;
}

DATABASE::DATABASE_CODONS& DATABASE::GetCodonsData()
{
	return codonsData;
}

DATABASE::DATABASE_AMINOACIDS& DATABASE::GetAcidsData()
{
	return acidsData;
}

DATABASE::DATABASE_AMINOACIDGROUPS& DATABASE::GetGroupsData()
{
	return groupsData;
}

DATABASE::DATABASE_PEPTIDES& DATABASE::GetPeptidesData()
{
	return peptidesData;
}

DATABASE::DATABASE_DNAS& DATABASE::GetDNAsData()
{
	return dnasData;
}

DATABASE::DATABASE_RNAS& DATABASE::GetRNAsData()
{
	return rnasData;
}

DATABASE::DATABASE_PEPTIDECLASSES& DATABASE::GetPeptideClassesData()
{
	return peptideClassesData;
}

DATABASE::GRANTSAMTABLE& DATABASE::GetGrantsamTable()
{
	return grantsamTable;
}

std::vector <std::string> DATABASE::AlignLines(
		const SHORT alignObjectType,
		const LONG index1,
		const LONG index2)
{
	switch(alignObjectType)
	{
		case ALIGN_PEPTIDES:
			return aligner.AlignPeptides(index1, index2);
			break;

		case ALIGN_DNAS:
			return aligner.AlignDNAs(index1, index2);
			break;

		default:
			std::vector <std::string> fakeResult;
			fakeResult.push_back("");
			fakeResult.push_back("");
			
			return fakeResult;
			break;
	}
}



DATABASE::~DATABASE()
{
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_NUCLEOTIDES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_NUCLEOTIDES::DATABASE_NUCLEOTIDES()
{
	fileName = "";
	database = NULL;
}

DATABASE::DATABASE_NUCLEOTIDES::DATABASE_NUCLEOTIDES(
		const DATABASE::DATABASE_NUCLEOTIDES& other): database(other.database)
{
	fileName = other.fileName;
	tableCosts = other.tableCosts;
}

DATABASE::DATABASE_NUCLEOTIDES::DATABASE_NUCLEOTIDES(
		const std::string newFileName,
		DATABASE* newdatabase)
{
	fileName = newFileName;

	database = newdatabase;
}

DATABASE::DATABASE_NUCLEOTIDES& DATABASE::DATABASE_NUCLEOTIDES::operator=(
		const DATABASE::DATABASE_NUCLEOTIDES& other)
{
	this->fileName = other.fileName;
	this->tableCosts = other.tableCosts;
	
	this->database = other.database;
	

	return *this;
}


NUCLEOTIDE& DATABASE::DATABASE_NUCLEOTIDES::GetNucleotideFromLetter(
		const char letter)
{
	for(SHORT i = 0; i < nucleotides.size(); i++)
		if((char)nucleotides[i] == letter)
			return nucleotides[i];

	return nucleotides[0];
}

bool DATABASE::DATABASE_NUCLEOTIDES::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::DATABASE_NUCLEOTIDES::Load()
{
	std::ifstream file;
	std::string line, subline;
	SHORT separatorPosition;

	std::string nucleotideName;
	char nucleotideLetter;
	bool nucleotideType;
	SHORT nucleotidePairInDNA;
	SHORT nucleotidePairInRNA;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла

	// считывание справочной информации о нуклеотидах
	SHORT currentNucleotide = 0;
	while(currentNucleotide < NUCLEOTIDES_NUMBER)
	{
		std::getline(file, line);
	
		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;

		nucleotideName = line;
		
		std::getline(file, line);
		nucleotideLetter = line[0];
				
		std::getline(file, line);
		nucleotideType = (bool) atoi(line.data());

		std::getline(file, line);
		nucleotidePairInDNA = atoi(line.data());

		std::getline(file, line);
		nucleotidePairInRNA = atoi(line.data());
		

		nucleotides.push_back(NUCLEOTIDE(currentNucleotide, nucleotideLetter, nucleotideName, nucleotideType, nucleotidePairInDNA, nucleotidePairInRNA, database));
		currentNucleotide++;
	}
	
	// получение таблицы замен
	currentNucleotide = 0;
	std::vector <int> currentNucleotideLine;
	while(currentNucleotide < NUCLEOTIDES_NUMBER)
	{
		
		std::getline(file, line);

		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;


		// буква в начале строки отбрасывается
		line = line.substr(3);
		currentNucleotideLine.clear();
		subline.clear();
		do
		{
			// вырезание индексов процентов сходства из строки
			separatorPosition = line.find_first_of(' ');
			if(separatorPosition != 65535)
			{
				subline = line.substr(0, separatorPosition);
				line = line.substr(separatorPosition + 1);
				separatorPosition = line.find_first_not_of(' ');
				line = line.substr(separatorPosition);
			}
			else
				subline = line;

			currentNucleotideLine.push_back(atoi(subline.data()));
		}
		while(line != subline);

		currentNucleotideLine.push_back(atoi(subline.data()));

		tableCosts.push_back(currentNucleotideLine);
		currentNucleotide++;
	}



	file.close();
	std::cout << "DATABASE: NUCLEOTIDES is succesfully loaded.\n";
	return true;
}

bool DATABASE::DATABASE_NUCLEOTIDES::SaveToFile(
		const std::string fileName)
{
	return true;
}


NUCLEOTIDE& DATABASE::DATABASE_NUCLEOTIDES::GetPairInDNA(
		const SHORT nucleotideIndex)
{
	return nucleotides[nucleotides[nucleotideIndex].GetPairInDNAIndex()];
}

SHORT DATABASE::DATABASE_NUCLEOTIDES::GetPairIndexInDNA(
		const SHORT nucleotideIndex)
{
	return nucleotides[nucleotideIndex].GetPairInDNAIndex();
}

NUCLEOTIDE& DATABASE::DATABASE_NUCLEOTIDES::GetPairInRNA(
		const SHORT nucleotideIndex)
{
	return nucleotides[nucleotides[nucleotideIndex].GetPairInRNAIndex()];
}

SHORT DATABASE::DATABASE_NUCLEOTIDES::GetPairIndexInRNA(
		const SHORT nucleotideIndex)
{
	return nucleotides[nucleotideIndex].GetPairInRNAIndex();
}


void DATABASE::DATABASE_NUCLEOTIDES::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                          DATABASE: NUCLEOTIDES\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(SHORT i = 0; i < nucleotides.size(); i++)
	{
		ShowNucleotideFromIndex(i);
		std::cout << "-------------------------------------------------------------------------------\n";
	}

	std::cout << "-------------------------------------------------------------------------------\n";
}

MUTATION_NUCLEOTIDE DATABASE::DATABASE_NUCLEOTIDES::DetermineNucleotideMutation(
		const SHORT nucleotide1Index, 
		const SHORT nucleotide2Index, 
		const LONG position = 0)
{
	MUTATION_NUCLEOTIDE mutation(nucleotide1Index, nucleotide2Index, position, database);

	return mutation;
}

NUCLEOTIDE& DATABASE::DATABASE_NUCLEOTIDES::operator[](
		const SHORT index)
{
	return this->GetNucleotideFromIndex(index);
}

NUCLEOTIDE& DATABASE::DATABASE_NUCLEOTIDES::GetNucleotideFromIndex(
		const SHORT index)
{
	if(index < nucleotides.size())
		return nucleotides[index];
	else
		return nucleotides[0];
}


void DATABASE::DATABASE_NUCLEOTIDES::ShowNucleotideFromLetter(
		const char letter)
{
	ShowNucleotideFromIndex(GetNucleotideFromLetter(letter));
}


void DATABASE::DATABASE_NUCLEOTIDES::ShowNucleotideFromIndex(
		const SHORT index)
{
	SHORT realIndex = index;
	if(realIndex >= nucleotides.size())
		realIndex = 0;

	std::cout << "NAME: " << (std::string)nucleotides[realIndex] << "\n";
	std::cout << "LETTER: " << (char)nucleotides[realIndex] << "\n";
	std::cout << "TYPE: " << (nucleotides[realIndex].TypeIsPurine() ? "purine" : "pyrimindine") << "\n";
	std::cout << "PAIR IN DNA: " << (std::string)nucleotides[realIndex].GetPairInDNA() << "\n";
	std::cout << "PAIR IN RNA: " << (std::string)nucleotides[realIndex].GetPairInDNA() << "\n";
}


int DATABASE::DATABASE_NUCLEOTIDES::GetChangingCost(
		const SHORT nucleotide1Index,
		const SHORT nucleotide2Index)
{
	/*
		-	@		U		C		A		G		T
		@	-1000	-1000	-1000	-1000	-1000	-1000
		U	-1000	8		0		-4		-3		8
		C	-1000	0		9		-3		-5		0
		A	-1000	-4		-3		10		-1		-4
		G	-1000	-3		-5		-1		7		-3
		T	-1000	8		0		-4		-3		8
															*/

	return tableCosts[nucleotide1Index][nucleotide2Index];
}


DATABASE::DATABASE_NUCLEOTIDES::~DATABASE_NUCLEOTIDES()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_CODONS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_CODONS::DATABASE_CODONS()
{
	fileName = "";

	database = NULL;
}

DATABASE::DATABASE_CODONS::DATABASE_CODONS(
		const DATABASE_CODONS& other): database(other.database)
{
	fileName = other.fileName;
}

DATABASE::DATABASE_CODONS::DATABASE_CODONS(
		const std::string newFileName,
		DATABASE* newdatabase)
{
	fileName = newFileName;

	database = newdatabase;
}

DATABASE::DATABASE_CODONS& DATABASE::DATABASE_CODONS::operator=(
		const DATABASE::DATABASE_CODONS& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}



bool DATABASE::DATABASE_CODONS::SaveToFile(
		const std::string fileName)
{
	return true;
}

bool DATABASE::DATABASE_CODONS::Load()
{
	std::ifstream file;
	std::string line;
	
	SHORT currentCodon = 0;

	SHORT codonAcidIndex;
	std::vector <SHORT> codonNucleotidesIndices;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);
	
		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;

		codonNucleotidesIndices.clear();
		codonNucleotidesIndices.push_back(SHORT(line[0] - CHAR_ZERO));
		codonNucleotidesIndices.push_back(SHORT(line[2] - CHAR_ZERO));
		codonNucleotidesIndices.push_back(SHORT(line[4] - CHAR_ZERO));
		
		std::getline(file, line);
		codonAcidIndex = atoi(line.data());
		

		codons.push_back(CODON(currentCodon, codonAcidIndex, codonNucleotidesIndices, database));
		currentCodon++;
	}


	file.close();
	std::cout << "DATABASE: CODONS is succesfully loaded.\n";
	return true;
}

void DATABASE::DATABASE_CODONS::ShowCodonFromIndex(
		SHORT index)
{
	SHORT realIndex = index;
	if(realIndex >= codons.size())
		realIndex = 0;

	std::cout << "NUCLEOTIDES: " << (std::string)codons[realIndex] << "\n";
	std::cout << "AMINOACID: " << (std::string)codons[realIndex].GetAcid() << "\n";
	std::cout << "-------------------------------------------------------------------------------\n";
}

void DATABASE::DATABASE_CODONS::ShowCodonFrom3Letters(
		const char letter1, 
		const char letter2, 
		const char letter3)
{
	ShowCodonFromIndex((SHORT)GetCodonFrom3Letters(letter1, letter2, letter3));
}

CODON& DATABASE::DATABASE_CODONS::GetCodonFrom3Letters(
		const char letter1, 
		const char letter2, 
		const char letter3)
{
	for(SHORT i = 0; i < codons.size(); i++)
		if(((char)codons[i].GetNucleotideAtPosition(0) == letter1) && ((char)codons[i].GetNucleotideAtPosition(1) == letter2) && ((char)codons[i].GetNucleotideAtPosition(2) == letter3))
			return codons[i];

	return codons[0];
}



void DATABASE::DATABASE_CODONS::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                              DATABASE: CODONS\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(SHORT i = 0; i < codons.size(); i++)
		ShowCodonFromIndex(i);

	std::cout << "-------------------------------------------------------------------------------\n";
}

bool DATABASE::DATABASE_CODONS::Save()
{
	return SaveToFile(fileName);
}

MUTATION_CODON DATABASE::DATABASE_CODONS::DetermineCodonMutation(
		const SHORT codon1Index, 
		const SHORT codon2Index, 
		const unsigned int position)
{
	MUTATION_CODON mutation(codon1Index, codon2Index, position, 1, database);

	return mutation;
}

CODON& DATABASE::DATABASE_CODONS::operator[](
		SHORT index)
{
	return codons[index];
}



DATABASE::DATABASE_CODONS::~DATABASE_CODONS()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_AMINOACIDS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_AMINOACIDS::DATABASE_AMINOACIDS()
{
	fileName = "";

	database = NULL;
}

DATABASE::DATABASE_AMINOACIDS::DATABASE_AMINOACIDS(
		const DATABASE::DATABASE_AMINOACIDS& other)
{
	fileName = other.fileName;

	database = other.database;
}

DATABASE::DATABASE_AMINOACIDS::DATABASE_AMINOACIDS(
		const std::string newFileName,
		DATABASE* newdatabase): database(newdatabase)
{
	fileName = newFileName;
}

DATABASE::DATABASE_AMINOACIDS& DATABASE::DATABASE_AMINOACIDS::operator=(
		const DATABASE::DATABASE_AMINOACIDS& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}


bool DATABASE::DATABASE_AMINOACIDS::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::DATABASE_AMINOACIDS::Load()
{
	std::ifstream file;
	std::string line, subline;
	
	SHORT currentAcid = 0, separatorPosition;

	std::string acidName;
	char acidLetter;
	SHORT acidGoupIndex;

	std::vector <SHORT> acidCodonsIndices;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);
	
		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;

		acidName = line;

		std::getline(file, line);
		acidLetter = line[0];

		std::getline(file, line);
		acidGoupIndex = atoi(line.data());
		
		std::getline(file, line);
		acidCodonsIndices.clear();

		do
		{
			// вырезание индексов кодонов из строки
			separatorPosition = line.find_first_of(' ');
			if(separatorPosition != -1)
			{
				subline = line.substr(0, separatorPosition);
				if(separatorPosition <= line.length())
					line = line.substr(separatorPosition + 1);
				else
					line = "";
			}

			acidCodonsIndices.push_back(atoi(subline.data()));
		}
		while(line.length() > 0);
		

		acids.push_back(AMINOACID(currentAcid, acidLetter, acidName, acidGoupIndex, acidCodonsIndices, database));
		currentAcid++;
	}


	file.close();
	std::cout << "DATABASE: AMINOACIDS is succesfully loaded.\n";
	return true;
}

bool DATABASE::DATABASE_AMINOACIDS::SaveToFile(
		const std::string fileName)
{
	return true;
}

void DATABASE::DATABASE_AMINOACIDS::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                             DATABASE: AMINOACIDS\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(SHORT i = 0; i < acids.size(); i++)
	{
		ShowAcidFromIndex(i);
	}

	std::cout << "-------------------------------------------------------------------------------\n";
}

AMINOACID& DATABASE::DATABASE_AMINOACIDS::GetAcidFromLetter(
		const char letter)
{
	for(SHORT i = 0; i < acids.size(); i++)
		if((char)acids[i] == letter)
			return acids[i];

	return acids[0];
}
				
MUTATION_AMINOACID DATABASE::DATABASE_AMINOACIDS::DetermineAcidMutation(
		const SHORT acid1Index, 
		const SHORT acid2Index, 
		const unsigned int position)
{
	MUTATION_AMINOACID mutation(acid1Index, acid2Index, position, 1, database);

	return mutation;
}

AMINOACID& DATABASE::DATABASE_AMINOACIDS::operator[](
		SHORT index)
{
	return acids[index];
}


SHORT DATABASE::DATABASE_AMINOACIDS::GetAcidsNumber()
{
	return acids.size();
}


void DATABASE::DATABASE_AMINOACIDS::ShowAcidFromIndex(
		const SHORT index)
{
	SHORT realIndex = index;
	if(realIndex >= acids.size())
		realIndex = 0;

	std::cout << "NAME: " << (std::string)acids[realIndex] << "\n";
	std::cout << "LETTER: " << (char)acids[realIndex] << "\n";
	std::cout << "GROUP: " << (std::string)acids[realIndex].GetGroup() << "\n";
		
	std::cout << "CODONS: ";
	for(SHORT j = 0; j < acids[realIndex].GetCodonsNumber(); j++)
		std::cout << (std::string)acids[realIndex][j] << "  ";
	std::cout << "\n";

	std::cout << "-------------------------------------------------------------------------------\n";
}

void DATABASE::DATABASE_AMINOACIDS::ShowAcidFromLetter(
		const char letter)
{
	ShowAcidFromIndex((SHORT)GetAcidFromLetter(letter));
}


DATABASE::DATABASE_AMINOACIDS::~DATABASE_AMINOACIDS()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_AMINOACIDGROUPS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_AMINOACIDGROUPS::DATABASE_AMINOACIDGROUPS()
{
	fileName = "";

	database = NULL;
}

DATABASE::DATABASE_AMINOACIDGROUPS::DATABASE_AMINOACIDGROUPS(
		const DATABASE::DATABASE_AMINOACIDGROUPS& other): database(other.database)
{
	fileName = other.fileName;
}

DATABASE::DATABASE_AMINOACIDGROUPS::DATABASE_AMINOACIDGROUPS(
		const std::string newFileName,
		DATABASE* newdatabase): database(newdatabase)
{
	fileName = newFileName;
}

DATABASE::DATABASE_AMINOACIDGROUPS& DATABASE::DATABASE_AMINOACIDGROUPS::operator=(
		const DATABASE::DATABASE_AMINOACIDGROUPS& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}


bool DATABASE::DATABASE_AMINOACIDGROUPS::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::DATABASE_AMINOACIDGROUPS::Load()
{
	std::ifstream file;
	std::string line, subline;
	
	SHORT currentGroup = 0, separatorPosition;

	std::string groupName;
	std::vector <SHORT> groupAcidsIndices;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);
	
		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;

		groupName = line;

		std::getline(file, line);
		groupAcidsIndices.clear();

		do
		{
			// вырезание индексов кодонов из строки
			separatorPosition = line.find_first_of(' ');
			if(separatorPosition != -1)
			{
				subline = line.substr(0, separatorPosition);
				if(separatorPosition <= line.length())
					line = line.substr(separatorPosition + 1);
				else
					line = "";
			}

			groupAcidsIndices.push_back(atoi(subline.data()));
		}
		while(line.length() > 0);
		

		groups.push_back(AMINOACIDGROUP(currentGroup, groupName, groupAcidsIndices, database));
		currentGroup++;
	}


	file.close();
	std::cout << "DATABASE: GROUPS OF AMINOACIDS is succesfully loaded.\n";
	return true;
}

bool DATABASE::DATABASE_AMINOACIDGROUPS::SaveToFile(
		const std::string fileName)
{
	return false;
}

void DATABASE::DATABASE_AMINOACIDGROUPS::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                        DATABASE: GROUPS OF AMINOACIDS\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(SHORT i = 0; i < groups.size(); i++)
	{
		ShowGroupFromIndex(i);
	}

	std::cout << "-------------------------------------------------------------------------------\n";
}

AMINOACIDGROUP& DATABASE::DATABASE_AMINOACIDGROUPS::operator[](
		const SHORT index)
{
	if(index < groups.size())
		return groups[index];
	else
		return groups[0];
}


void DATABASE::DATABASE_AMINOACIDGROUPS::ShowGroupFromIndex(
		const SHORT index)
{
	SHORT realIndex = index;
	if(realIndex >= groups.size())
		realIndex = 0;

	std::cout << "NAME: " << (std::string)groups[realIndex] << "\n";	
	std::cout << "AMINOACIDS: ";
	for(SHORT j = 0; j < groups[realIndex].GetAcidsNumber(); j++)
		std::cout << (char)groups[realIndex][j] << " ";
	std::cout << "\n";

	std::cout << "-------------------------------------------------------------------------------\n";
}


DATABASE::DATABASE_AMINOACIDGROUPS::~DATABASE_AMINOACIDGROUPS()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_PEPTIDES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_PEPTIDES::DATABASE_PEPTIDES()
{
	fileName = "";

	database = NULL;
}

DATABASE::DATABASE_PEPTIDES::DATABASE_PEPTIDES(
		const DATABASE::DATABASE_PEPTIDES& other): database(other.database)
{
	fileName = other.fileName;
}

DATABASE::DATABASE_PEPTIDES::DATABASE_PEPTIDES(
		const std::string newFileName,
		DATABASE* newdatabase): database(newdatabase)
{
	fileName = newFileName;
}

DATABASE::DATABASE_PEPTIDES& DATABASE::DATABASE_PEPTIDES::operator=(
		const DATABASE::DATABASE_PEPTIDES& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}


bool DATABASE::DATABASE_PEPTIDES::Load()
{
	if(AddFromFile(fileName, DATABASE_FIRSTLOAD))
	{
		std::cout << "DATABASE: PEPTIDES is succesfully loaded.\n";
		return true;
	}
	else
	{
		std::cout << "DATABASE: PEPTIDES failed to load.\n";
		return false;
	}
}

bool DATABASE::DATABASE_PEPTIDES::AddFromFile(
		const std::string fileName,
		bool firstLoad)
{
	std::ifstream file;
	std::string line, currentName, currentLine;

	LONG currentPeptide = 0;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);
	
		// пустые строки и строки с комментариями игнорируются
		if(!line.length())
			continue;
		if(line[0] == '/')
			continue;
		

		currentName = line;

		std::getline(file, currentLine);
		
		// если пептид добавляется из исходной базы, то нет вопросов. Иначе дополнительная проверка - вдруг уже есть пептид с таким именем?
		if(firstLoad)
			peptides.push_back(PEPTIDE(currentPeptide, 0, currentName, currentLine, database));
		else
			Add(PEPTIDE(currentPeptide, 0, currentName, currentLine, database));

		currentPeptide++;
	}

	std::cout << "DATABASE: PEPTIDES - peptides from file " << fileName << " are succesfully added.\n";
	file.close();


	Save();
	return true;
}

bool DATABASE::DATABASE_PEPTIDES::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::DATABASE_PEPTIDES::SaveToFile(
		const std::string fileName)
{
	std::ofstream file;

	file.open(fileName);
	if(!file.is_open())
		return false;

	// печать шапки с информацией о содержимом файла
	file << "// База данных: пептиды\n";
	file << "//  Поля:\n";
	file << "// 1. Имя\n";
	file << "// 2. Последовательность аминокислот (большие буквы)\n";
	file << "//\n";

	for(LONG i = 0; i < peptides.size(); i++)
	{
		file << peptides[i].GetName() << "\n";
		file << (std::string)peptides[i] << "\n";

		// пустая строка - визуальный разделитель пептидов
		file << "\n";
	}


	file.close();
	return true;
}

bool DATABASE::DATABASE_PEPTIDES::Add(
		PEPTIDE newPeptide)
{
	try
	{
		for(LONG i = 0; i < peptides.size(); i++)
			if(::LinesAreEqual(peptides[i].GetName(), newPeptide.GetName()))
			{
				std::cout << "Peptide with name '" << newPeptide.GetName() << "' is already in base.\n";
				return false;
			}

		// если пептида с таким именем не найдено - добавить
		peptides.push_back(newPeptide);
		return true;
	}
	catch(...){}

	return false;
}

bool DATABASE::DATABASE_PEPTIDES::AddFromKeyboard()
{
	std::string name, line;

	std::system("cls");
	std::cout << "ADD NEW PEPTIDE:\n\n";

	std::cout << "Enter name: ";
	std::cin.ignore(1);
	std::getline(std::cin, name);
	
	if(LinesAreSimilar(name, "fuck!"))
	{
		std::cout << "I understand, you don't want to add this peptide. OK!\n";
		
		return false; 
	}
	else
	{
		std::cout << "Enter line of aminoacids: ";
		std::cin.ignore(1);
		std::getline(std::cin, line);

		// проверка введённой строки. Символы, не соответствующие кислотам, заменить на символ левой кислоты
		for(LONG i = 0; i < line.length(); i++)
			if(!(SHORT)database->acidsData.GetAcidFromLetter(line[i]))
				line[i] = (char)database->acidsData[i];


		bool result = Add(PEPTIDE(peptides.size(), 0, name, line, database));
		Save();

		return result;
	}
}
			

LONG DATABASE::DATABASE_PEPTIDES::GetTotalAcidsNumber(
		const SHORT acidIndex)
{
	LONG result = 0;

	for(LONG i = 0; i < peptides.size(); i++)
		result += peptides[i].GetAcidNumber(acidIndex);

	return result;
}
				
double DATABASE::DATABASE_PEPTIDES::GetTotalAcidsStake(
		const SHORT acidIndex)
{
	double result = 0.0;
	LONG number = 0, totalNumber = 0;

	for(LONG i = 0; i < peptides.size(); i++)
	{
		number += peptides[i].GetAcidNumber(acidIndex);
		totalNumber += peptides[i].GetLength();
	}

	result = (double)(number / totalNumber);

	return result;
}

void DATABASE::DATABASE_PEPTIDES::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                           DATABASE: PEPTIDES [" << GetSize() << "]\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(LONG i = 1; i < peptides.size(); i++)
	{
		ShowPeptideFromIndex(i);
	}

	std::cout << "-------------------------------------------------------------------------------\n";
}

MUTATION_PEPTIDE DATABASE::DATABASE_PEPTIDES::DeterminePeptideMutation(
		const LONG peptide1Index, 
		const LONG peptide2Index)
{
	MUTATION_PEPTIDE mutation(peptide1Index, peptide2Index, database);
	return mutation;
}

MUTATION_PEPTIDE DATABASE::DATABASE_PEPTIDES::GetFakeMutation()
{
	MUTATION_PEPTIDE fakeMutation(0, 0, database);
	return fakeMutation;
}


PEPTIDE& DATABASE::DATABASE_PEPTIDES::operator[](
		LONG index)
{
	unsigned realIndex = index;

	if(realIndex >= peptides.size())
		realIndex = 0;

	return peptides[realIndex];
}


void DATABASE::DATABASE_PEPTIDES::ShowPeptideFromIndex(
		LONG index)
{
	std::cout << "NAME: " << operator[](index).GetName() << "\n";
	std::cout << "LINE: " << (std::string)operator[](index) << "\n";
	std::cout << operator[](index).GetLength() << " AA\n";
	std::cout << "-------------------------------------------------------------------------------\n";
}

void DATABASE::DATABASE_PEPTIDES::ShowPeptideFromName(
		const std::string name)
{
	ShowPeptideFromIndex((LONG)GetPeptideFromName(name));
}

PEPTIDE& DATABASE::DATABASE_PEPTIDES::GetPeptideFromName(
		const std::string name)
{
	for(LONG i = 0; i < peptides.size(); i++)
		if(LinesAreEqual(name, peptides[i].GetName()))
			return peptides[i];

	return peptides[0];
}


LONG DATABASE::DATABASE_PEPTIDES::GetSize()
{
	return peptides.size() - 1;
}


std::vector <std::string> DATABASE::DATABASE_PEPTIDES::SearchPeptideNames(
		const std::string searchingName)
{
	std::vector <std::string> results;

	for(LONG i = 1; i < peptides.size(); i++)
	{
		if(LinesAreSimilar(peptides[i].GetName(), searchingName))
			results.push_back(peptides[i].GetName());
	}

	return results;
}

PEPTIDE& DATABASE::DATABASE_PEPTIDES::SearchPeptidesAndChooseOne(
		const std::string line)
{
	std::vector <std::string> lines;

	lines = SearchPeptideNames(line);


	if(lines.empty())
	{
		std::cout << "No peptides found from search '" << line << "'.\n";
		return peptides[0];
	}
	else
		if(lines.size() == 1)
			return GetPeptideFromName(lines[0]);
		else
		{
			// найдено много пептидов с подходящим названием. Вывести все и предложить выбор
			std::cout << "Here are " << lines.size() << " peptides found. Choose one:\n\n";
						
			for(LONG i = 0; i < lines.size(); i++)
				std::cout << EnterSpacesForGoodNumberReading(i + 1, lines.size() - 1) << i + 1 << ". " << lines[i] << " - " << GetPeptideFromName(lines[i]).GetLength() << " AA\n";

			std::cout << "Your choice: ";
			LONG index;
			std::cin >> index;
			std::cin.ignore(1);

			if(index)
			{
				if(index <= lines.size())
					return GetPeptideFromName(lines[index - 1]);
				else
					return GetPeptideFromName(lines[lines.size() - 1]);
			}
			else
				return peptides[0];
		}
}


			
DATABASE::DATABASE_PEPTIDES::~DATABASE_PEPTIDES()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_DNAS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_DNAS::DATABASE_DNAS()
{
	fileName = "";

	database = NULL;
}
		
DATABASE::DATABASE_DNAS::DATABASE_DNAS(
		const DATABASE::DATABASE_DNAS& other): database(other.database)
{
	fileName = other.fileName;
}

DATABASE::DATABASE_DNAS::DATABASE_DNAS(
		const std::string newFileName,
		DATABASE* newdatabase): database(newdatabase)
{
	fileName = newFileName;
}

DATABASE::DATABASE_DNAS& DATABASE::DATABASE_DNAS::operator=(
		const DATABASE::DATABASE_DNAS& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}


bool DATABASE::DATABASE_DNAS::Load()
{
	if(AddFromFile(fileName, DATABASE_FIRSTLOAD))
	{
		std::cout << "DATABASE: PEPTIDES is succesfully loaded.\n";
		return true;
	}
	else
	{
		std::cout << "DATABASE: PEPTIDES failed to load.\n";
		return false;
	}
}
		
bool DATABASE::DATABASE_DNAS::Save()
{
	return SaveToFile(fileName);
}
		
bool DATABASE::DATABASE_DNAS::SaveToFile(
		const std::string fileName)
{
	std::ofstream file;

	file.open(fileName);
	if(!file.is_open())
		return false;

	// печать шапки с информацией о содержимом файла
	file << "// База данных: ДНК\n";
	file << "//  Поля:\n";
	file << "// 1. Имя\n";
	file << "// 2. Последовательность нуклеотидов (большие буквы)\n";
	file << "//\n";

	for(LONG i = 0; i < dnas.size(); i++)
	{
		file << dnas[i].GetName() << "\n";
		file << (std::string)dnas[i] << "\n";

		// пустая строка - визуальный разделитель ДНК
		file << "\n";
	}


	file.close();
	return true;
}
		
bool DATABASE::DATABASE_DNAS::Add(
		DNA newDNA)
{
	try
	{
		for(LONG i = 0; i < dnas.size(); i++)
			if(::LinesAreEqual(dnas[i].GetName(), newDNA.GetName()))
			{
				std::cout << "DNA with name '" << newDNA.GetName() << "' is already in base.\n";
				return false;
			}

		// если ДНК с таким именем не найдено - добавить
		dnas.push_back(newDNA);
		return true;
	}
	catch(...){}

	return false;
}

bool DATABASE::DATABASE_DNAS::AddFromKeyboard()
{
	std::string name, line;

	std::system("cls");
	std::cout << "ADD NEW DNA:\n\n";

	std::cout << "Enter name: ";
	std::cin.ignore(1);
	std::getline(std::cin, name);
	
	if(LinesAreSimilar(name, "fuck!"))
	{
		std::cout << "I understand, you don't want to add this DNA. OK!\n";
		
		return false; 
	}
	else
	{
		std::cout << "Enter line of nucleotides: ";
		//std::cin.ignore(1);
		std::getline(std::cin, line);

		// проверка введённой строки. Символы, не соответствующие нуклеотидам, заменить на символ левого нуклеотида
		for(LONG i = 0; i < line.length(); i++)
			if(!(SHORT)database->nucleotidesData.GetNucleotideFromLetter(line[i]))
				line[i] = (char)database->nucleotidesData[0];


		bool result = Add(DNA(dnas.size(), 0, 0, 0, name, line, database));
		Save();

		return result;
	}
}
		
bool DATABASE::DATABASE_DNAS::AddFromFile(
		const std::string fileName,
		bool firstLoad)
{
	std::ifstream file;
	std::string line, currentName, currentLine;

	LONG currentDNA = 0;


	file.open(fileName);
	if(!file.is_open())
		return false;


	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);
	
		// пустые строки и строки с комментариями игнорируются
		if(!line.length())
			continue;
		if(line[0] == '/')
			continue;
		

		currentName = line;

		std::getline(file, currentLine);
		
		// если ДНК добавляется из исходной базы, то нет вопросов. Иначе дополнительная проверка - вдруг уже есть ДНК с таким именем?
		if(firstLoad)
			dnas.push_back(DNA(currentDNA, 0, 0, 0, currentName, currentLine, database));
		else
			Add(DNA(currentDNA, 0, 0, 0, currentName, currentLine, database));

		currentDNA++;
	}

	std::cout << "DATABASE: DNAS - DNAs from file " << fileName << " are succesfully added.\n";
	file.close();


	Save();
	return true;
}
		
DNA& DATABASE::DATABASE_DNAS::operator[](
		const LONG index)
{
	if(index < dnas.size())
		return dnas[index];
	else
		return dnas[0];
}

void DATABASE::DATABASE_DNAS::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                             DATABASE: DNAS [" << GetSize() << "]\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(LONG i = 1; i < dnas.size(); i++)
	{
		ShowDNAFromIndex(i);
	}

	std::cout << "-------------------------------------------------------------------------------\n";
}

void DATABASE::DATABASE_DNAS::ShowDNAFromIndex(
		const LONG index)
{
	std::cout << "NAME: " << operator[](index).GetName() << "\n";
	std::cout << "LINE: " << (std::string)operator[](index) << "\n";
	std::cout << operator[](index).GetLength() << " nucleotides\n";
	std::cout << "BELOSERSKY COEFFICIENT: " << operator[](index).CalculateBeloserskyCoefficient() << "\n";
	std::cout << "-------------------------------------------------------------------------------\n";
}

void DATABASE::DATABASE_DNAS::ShowDNAFromName(
		const std::string name)
{
	ShowDNAFromIndex((LONG)GetDNAFromName(name));
}

DNA& DATABASE::DATABASE_DNAS::GetDNAFromName(
		const std::string name)
{
	for(LONG i = 0; i < dnas.size(); i++)
		if(LinesAreEqual(name, dnas[i].GetName()))
			return dnas[i];

	return dnas[0];
}

LONG DATABASE::DATABASE_DNAS::GetSize()
{
	return dnas.size() - 1;
}


std::vector <std::string> DATABASE::DATABASE_DNAS::SearchDNANames(
		const std::string searchingName)
{
	std::vector <std::string> results;

	for(LONG i = 1; i < dnas.size(); i++)
	{
		if(LinesAreSimilar(dnas[i].GetName(), searchingName))
			results.push_back(dnas[i].GetName());
	}

	return results;
}


DNA& DATABASE::DATABASE_DNAS::SearchDNAsAndChooseOne(
		const std::string line)
{
	std::vector <std::string> lines;

	lines = SearchDNANames(line);


	if(lines.empty())
	{
		std::cout << "No DNAs found from search '" << line << "'.\n";
		return dnas[0];
	}
	else
		if(lines.size() == 1)
			return GetDNAFromName(lines[0]);
		else
		{
			// найдено много пептидов с подходящим названием. Вывести все и предложить выбор
			std::cout << "Here are " << lines.size() << " DNAs found. Choose one:\n\n";
						
			for(LONG i = 0; i < lines.size(); i++)
				std::cout << EnterSpacesForGoodNumberReading(i + 1, lines.size() - 1) << i + 1 << ". " << lines[i] << " - " << GetDNAFromName(lines[i]).GetLength() << " nucleotides\n";

			std::cout << "Your choice: ";
			LONG index;
			std::cin >> index;
			std::cin.ignore(1);

			if(index)
			{
				if(index <= lines.size())
					return GetDNAFromName(lines[index - 1]);
				else
					return GetDNAFromName(lines[lines.size() - 1]);
			}
			else
				return dnas[0];
		}
}


MUTATION_DNA DATABASE::DATABASE_DNAS::DetermineDNAMutation(
		const LONG dna1Index, 
		const LONG dna2Index)
{
	MUTATION_DNA mutation(dna1Index, dna2Index, database);
	return mutation;
}

MUTATION_DNA DATABASE::DATABASE_DNAS::GetFakeMutation()
{
	MUTATION_DNA mutation(0, 0, database);
	return mutation;
}




long DATABASE::DATABASE_DNAS::GetTotalNucleotideNumber(
		const short nucleotideIndex)
{
	return totalNucleotidesNumbers[nucleotideIndex];
}



DATABASE::DATABASE_DNAS::~DATABASE_DNAS()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_RNAS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_RNAS::DATABASE_RNAS()
{
	fileName = "";

	database = NULL;
}
		
DATABASE::DATABASE_RNAS::DATABASE_RNAS(
		const  DATABASE::DATABASE_RNAS& other): database(other.database)
{
	fileName = other.fileName;
}

 DATABASE::DATABASE_RNAS::DATABASE_RNAS(
		const std::string newFileName,
		DATABASE* newdatabase): database(newdatabase)
{
	fileName = newFileName;
}

 DATABASE::DATABASE_RNAS& DATABASE::DATABASE_RNAS::operator=(
		const DATABASE::DATABASE_RNAS& other)
{
	this->fileName = other.fileName;
	this->database = other.database;

	return *this;
}


bool DATABASE::DATABASE_RNAS::Load()
{
	return true;
}
		
bool DATABASE::DATABASE_RNAS::Save()
{
	return SaveToFile(fileName);
}
		
bool DATABASE::DATABASE_RNAS::SaveToFile(
		const std::string fileName)
{
	return false;
}
		
bool DATABASE::DATABASE_RNAS::Add(
		RNA newRNA)
{
	try
	{
		rnas.push_back(newRNA);
		return true;
	}
	catch(...) {}

	return false;
}

bool DATABASE::DATABASE_RNAS::AddFromKeyboard()
{
	return false;
}
		
bool DATABASE::DATABASE_RNAS::AddFromFile(
		const std::string fileName)
{
	return false;
}
		
RNA& DATABASE::DATABASE_RNAS::operator[](
		const LONG index)
{
	return rnas[index];
}

void DATABASE::DATABASE_RNAS::ShowData()
{
}


long DATABASE::DATABASE_RNAS::GetTotalNucleotideNumber(
		const short nucleotideIndex)
{
	return totalNucleotidesNumbers[nucleotideIndex];
}



DATABASE::DATABASE_RNAS::~DATABASE_RNAS()
{
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_PEPTIDECLASSES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::DATABASE_PEPTIDECLASSES::DATABASE_PEPTIDECLASSES()
{
	database = NULL;

	fileName = "";
}

DATABASE::DATABASE_PEPTIDECLASSES::DATABASE_PEPTIDECLASSES(
		const DATABASE_PEPTIDECLASSES& other)
{
	database = other.database;

	fileName = other.fileName;
}

DATABASE::DATABASE_PEPTIDECLASSES::DATABASE_PEPTIDECLASSES(
		const std::string newFileName,
		DATABASE* newdatabase)
{
	database = newdatabase;

	fileName = newFileName;
}

DATABASE::DATABASE_PEPTIDECLASSES& DATABASE::DATABASE_PEPTIDECLASSES::operator=(
		const DATABASE_PEPTIDECLASSES& other)
{
	database = other.database;

	fileName = other.fileName;

	return *this;
}



bool DATABASE::DATABASE_PEPTIDECLASSES::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::DATABASE_PEPTIDECLASSES::Load()
{
	return true;
}


bool DATABASE::DATABASE_PEPTIDECLASSES::SaveToFile(
		const std::string fileName)
{
	return false;
}


void DATABASE::DATABASE_PEPTIDECLASSES::ShowData()
{

}


PEPTIDECLASS& DATABASE::DATABASE_PEPTIDECLASSES::operator[](
		const LONG index)
{
	return classes[index];
}
				


DATABASE::DATABASE_PEPTIDECLASSES::~DATABASE_PEPTIDECLASSES()
{
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС DATABASE_PEPTIDECLASSES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DATABASE::GRANTSAMTABLE::GRANTSAMTABLE()
{
	fileName = "";
	
	database = NULL;
}

DATABASE::GRANTSAMTABLE::GRANTSAMTABLE(
		const GRANTSAMTABLE& other)
{
	this->fileName = other.fileName;

	this->table = other.table;

	this->database = other.database;
}

DATABASE::GRANTSAMTABLE::GRANTSAMTABLE(
		const std::string newFileName,
		DATABASE* newdatabase)
{
	this->fileName = std::string(newFileName);

	this->database = newdatabase;
}

DATABASE::GRANTSAMTABLE& DATABASE::GRANTSAMTABLE::operator=(
		const DATABASE::GRANTSAMTABLE& other)
{
	this->fileName = other.fileName;

	this->table = other.table;

	this->database = other.database;


	return *this;
}



bool DATABASE::GRANTSAMTABLE::Load()
{
	std::ifstream file;
	std::string line, subline;
	std::vector <double> currentAcidLine;
	SHORT lineNumber = 0, separatorPosition;

	file.open(fileName);
	if(!file.is_open())
	{
		std::cout << "FUCKIN' PROBLEMS WITH OPENING FILE " << fileName << "\n";
		return false;
	}

	// обработка файла
	while(!file.eof())
	{
		std::getline(file, line);

		// строки с комментариями игнорируются
		if(line[0] == '/')
			continue;


		// буква в начале строки отбрасывается
		line = line.substr(3);
		currentAcidLine.clear();
		subline.clear();
		do
		{
			// вырезание индексов процентов сходства из строки
			separatorPosition = line.find_first_of(' ');
			if(separatorPosition != 65535)
			{
				subline = line.substr(0, separatorPosition);
				line = line.substr(separatorPosition + 1);
				separatorPosition = line.find_first_not_of(' ');
				line = line.substr(separatorPosition);
			}
			else
				subline = line;

			currentAcidLine.push_back(atoi(subline.data()) / 100.0);
		}
		while(line != subline);

		currentAcidLine.push_back(atoi(subline.data()) / 100.0);

		table.push_back(currentAcidLine);
		lineNumber++;
	}


	file.close();
	std::cout << "DATABASE: GRANTSAM' TABLE is successfully loaded.\n";
	return true;
}

bool DATABASE::GRANTSAMTABLE::Save()
{
	return SaveToFile(fileName);
}

bool DATABASE::GRANTSAMTABLE::SaveToFile(
		const std::string fileName)
{
	return false;
}


void DATABASE::GRANTSAMTABLE::ShowData()
{
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "                           DATABASE: GRANTSAM' TABLE\n";
	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";

	for(SHORT i = 0; i < database->acidsData.GetAcidsNumber(); i++)
		std::cout << "  " << (char)database->acidsData[i];
	std::cout << "\n";

	for(SHORT i = 0; i < database->acidsData.GetAcidsNumber(); i++)
	{
		std::cout << (char)database->acidsData[i] << " ";
		for(SHORT j = 0; j < database->acidsData.GetAcidsNumber(); j++)
			std::cout << table[i][j] << " ";
		std::cout << "\n";
	}

	std::cout << "-------------------------------------------------------------------------------\n";
	std::cout << "-------------------------------------------------------------------------------\n";
}


double DATABASE::GRANTSAMTABLE::GetSimilarity(
		SHORT acid1Index, 
		SHORT acid2Index)
{
	if(acid1Index < AMINOACIDS_NUMBER && acid2Index < AMINOACIDS_NUMBER)
		return table[acid1Index][acid2Index];
	else 
		return table[0][0];
}


bool DATABASE::GRANTSAMTABLE::MutationIsConservative(
		SHORT acid1Index, 
		SHORT acid2Index)
{
	return false;
}



DATABASE::GRANTSAMTABLE::~GRANTSAMTABLE()
{
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// КЛАСС LINEALIGNER
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


DATABASE::LINEALIGNER::LINEALIGNER()
{
	database = NULL;
}

DATABASE::LINEALIGNER::LINEALIGNER(
		const DATABASE::LINEALIGNER& other)
{
	database = other.database;
}

DATABASE::LINEALIGNER::LINEALIGNER(
		DATABASE* newdatabase)
{
	database = newdatabase;
}



std::vector <std::string> DATABASE::LINEALIGNER::AlignPeptides(
		const LONG peptide1Index,
		const LONG peptide2Index)
{
	// получение последовательностей
	std::string line1 = database->peptidesData[peptide1Index];
	std::string line2 = database->peptidesData[peptide2Index];

	
	std::vector <std::string> result;
	std::string result1 = "", result2 = "";


	// а вдруг?
	if(LinesAreEqual(line1, line2))
	{
		result.push_back(line1);
		result.push_back(line2);

		return result;
	}


	// создание матрицы Нидлмана
	double** matrixNeedleman = new double*[line1.length() + 1];
	for(LONG i = 0; i < line1.length() + 1; i++)
		matrixNeedleman[i] = new double[line2.length() + 1];

	// заполнение базиса
	for(LONG i = 0; i < line1.length() + 1; i++)
		matrixNeedleman[i][0] = ACID_GAPCOST * i;
	for(LONG i = 0; i < line2.length() + 1; i++)
		matrixNeedleman[0][i] = ACID_GAPCOST * i;

	// заполнение всей матрицы
	SHORT acid1Index, acid2Index;
	for(LONG i = 1; i < line1.length() + 1; i++)
		for(LONG j = 1; j < line2.length() + 1; j++)
		{
			acid1Index = (SHORT)database->acidsData.GetAcidFromLetter(line1[i - 1]);
			acid2Index = (SHORT)database->acidsData.GetAcidFromLetter(line2[j - 1]);

			double costMatch = matrixNeedleman[i - 1][j - 1] + database->grantsamTable.GetSimilarity(acid1Index, acid2Index);
			double costDelete = matrixNeedleman[i - 1][j] + ACID_GAPCOST;
			double costInsert = matrixNeedleman[i][j - 1] + ACID_GAPCOST;

			matrixNeedleman[i][j] = Max(costMatch, costDelete, costInsert);
		}

	// развёртка матрицы и получение выравнивания
	long i = line1.length(), j = line2.length();
	while(i && j)
	{
		acid1Index = (SHORT)database->acidsData.GetAcidFromLetter(line1[i - 1]);
		acid2Index = (SHORT)database->acidsData.GetAcidFromLetter(line2[j - 1]);

		double score = matrixNeedleman[i][j];
		double scoreDiag = matrixNeedleman[i - 1][j - 1];
		double scoreUp = matrixNeedleman[i][j - 1];
		double scoreLeft = matrixNeedleman[i - 1][j];

		if(score == scoreDiag + database->grantsamTable.GetSimilarity(acid1Index, acid2Index))
		{
			result1 = line1[i - 1] + result1;
			result2 = line2[j - 1] + result2;
			
			i--;
			j--;
		}
		else
			if(score == scoreLeft + ACID_GAPCOST)
			{
				result1 = line1[i - 1] + result1;
				result2 = " " + result2;

				i--;
			}
			else
			{
				result1 = " " + result1;
				result2 = line2[j - 1] + result2;

				j--;
			}
	}
	while(i)
	{
		result1 = line1[i - 1] + result1;
		result2 = " " + result2;

		i--;
	}
	while(j)
	{
		result1 = " " + result1;
		result2 = line2[j - 1] + result2;

		j--;
	}


	// отправка результатов
	result.push_back(result1);
	result.push_back(result2);

	return result;
}


// временно работает по таблице Грентсема
std::vector <std::string> DATABASE::LINEALIGNER::AlignDNAs(
		const LONG dna1Index,
		const LONG dna2Index)
{
	// получение последовательностей
	std::string line1 = (std::string)database->dnasData[dna1Index];
	std::string line2 =(std::string) database->dnasData[dna2Index];

	
	std::vector <std::string> result;
	std::string result1 = "", result2 = "";


	// а вдруг?
	if(LinesAreEqual(line1, line2))
	{
		result.push_back(line1);
		result.push_back(line2);

		return result;
	}


	// создание матрицы Нидлмана
	double** matrixNeedleman = new double*[line1.length() + 1];
	for(LONG i = 0; i < line1.length() + 1; i++)
		matrixNeedleman[i] = new double[line2.length() + 1];

	// заполнение базиса
	for(LONG i = 0; i < line1.length() + 1; i++)
		matrixNeedleman[i][0] = NUCLEOTIDE_GAPCOST * i;
	for(LONG i = 0; i < line2.length() + 1; i++)
		matrixNeedleman[0][i] = NUCLEOTIDE_GAPCOST * i;

	// заполнение всей матрицы
	SHORT nucleotide1Index, nucleotide2Index;
	for(LONG i = 1; i < line1.length() + 1; i++)
		for(LONG j = 1; j < line2.length() + 1; j++)
		{
			nucleotide1Index = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line1[i - 1]);
			nucleotide2Index = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line2[j - 1]);

			double costMatch = matrixNeedleman[i - 1][j - 1] + database->nucleotidesData.GetChangingCost(nucleotide1Index, nucleotide2Index);
			double costDelete = matrixNeedleman[i - 1][j] + NUCLEOTIDE_GAPCOST;
			double costInsert = matrixNeedleman[i][j - 1] + NUCLEOTIDE_GAPCOST;

			matrixNeedleman[i][j] = Max(costMatch, costDelete, costInsert);
		}

	// развёртка матрицы и получение выравнивания
	LONG i = line1.length(), j = line2.length();
	while(i && j)
	{
		nucleotide1Index = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line1[i - 1]);
		nucleotide2Index = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line2[j - 1]);

		double score = matrixNeedleman[i][j];
		double scoreDiag = matrixNeedleman[i - 1][j - 1];
		double scoreUp = matrixNeedleman[i][j - 1];
		double scoreLeft = matrixNeedleman[i - 1][j];

		if(score == scoreDiag + database->nucleotidesData.GetChangingCost(nucleotide1Index, nucleotide2Index))
		{
			result1 = line1[i - 1] + result1;
			result2 = line2[j - 1] + result2;
			
			i--;
			j--;
		}
		else
			if(score == scoreLeft + NUCLEOTIDE_GAPCOST)
			{
				result1 = line1[i - 1] + result1;
				result2 = " " + result2;

				i--;
			}
			else
			{
				result1 = " " + result1;
				result2 = line2[j - 1] + result2;

				j--;
			}
	}
	while(i)
	{
		result1 = line1[i - 1] + result1;
		result2 = " " + result2;

		i--;
	}
	while(j)
	{
		result1 = " " + result1;
		result2 = line2[j - 1] + result2;

		j--;
	}


	// отправка результатов
	result.push_back(result1);
	result.push_back(result2);

	return result;
}



DATABASE::LINEALIGNER::~LINEALIGNER()
{
}