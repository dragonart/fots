#pragma once
#include "fots_core.h"
#include "database.h"

class PEPTIDECLASS
{
	public:
		PEPTIDECLASS();
		PEPTIDECLASS(
				const PEPTIDECLASS& other);
		PEPTIDECLASS(
				const LONG index,
				const std::string name,
				std::vector <LONG> dnasIndices,
				DATABASE* database);


		LONG GetIndex();
		std::string GetName();
		
		LONG GetDNAsNumber();

		DNA& operator[](
				LONG indexInClass);
		DNA& GetDNA(
				LONG indexInClass);

		LONG GetDNAIndex(
				LONG indexInClass);

		
		~PEPTIDECLASS();


	private:
		LONG index;
		
		std::string name;

		std::vector <LONG> dnasIndices;

		DATABASE* database;
};

