#pragma once
#include "fots_core.h"
#include "database.h"


class AMINOACIDGROUP
{
	public:
		AMINOACIDGROUP();
		AMINOACIDGROUP(
				const AMINOACIDGROUP& other);
		AMINOACIDGROUP(
				const SHORT index,
				const std::string name,
				std::vector <SHORT> acidsIndices,
				DATABASE* database);


		AMINOACID& operator[](
				const SHORT acidIndexInGroup);
		AMINOACID& GetAcid(
				const SHORT acidIndexInGroup);
		
		SHORT GetAcidIndex(
				const SHORT acidIndexInGroup);

		SHORT GetAcidsNumber();
		SHORT GetCodonsNumber();

		operator SHORT() {return index;}
		operator std::string() {return name;}


		~AMINOACIDGROUP();


	private:
		SHORT index;
		
		std::string name;
		
		std::vector <SHORT> acidsIndices;

		DATABASE* database;
};