#pragma once
#include "fots_core.h"
#include "database.h"


class MUTATION_CODON
{
	public:
		MUTATION_CODON();
		MUTATION_CODON(
				const MUTATION_CODON& other);
		MUTATION_CODON(
				const SHORT codon1Index,
				const SHORT codon2Index,
				const unsigned int codonPosition, 
				const unsigned int peptideLength,
				DATABASE* database);
		
		MUTATION_CODON& operator=(
				const MUTATION_CODON& other);


		std::string GetProcess();
		
		double GetDifficulty();

		SHORT GetTransitionsNumber();
		SHORT GetTransversionsNumber();
		
		bool MutationChangesAcid();
		bool MutationChangesAcidGroup();

		double CalculateGrantsamImportance();

		void Show();


		~MUTATION_CODON();


	private:
		SHORT codon1Index;
		SHORT codon2Index;

		std::string process;

		unsigned int codonPosition;

		double difficulty;
		
		bool positionsChanged[NUCLEOTIDES_IN_CODON_NUMBER];
		
		SHORT transitionsNumber;
		SHORT transversionsNumber;
		
		DATABASE* database;
};