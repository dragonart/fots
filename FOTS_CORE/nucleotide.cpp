#include "nucleotide.h"
#include "database.h"


NUCLEOTIDE::NUCLEOTIDE()
{
	index = 0;

	letter = '@';
	name = "empty nucleotide";

	typeIsPurine = true;
	
	pairInDNAIndex = 0;
	pairInRNAIndex = 0;

	database = NULL;
}

NUCLEOTIDE::NUCLEOTIDE(
		const NUCLEOTIDE& other)
{
	index = other.index;

	letter = other.letter;
	name = other.name;

	typeIsPurine = other.typeIsPurine;
	
	pairInDNAIndex = other.pairInDNAIndex;
	pairInRNAIndex = other.pairInRNAIndex;

	database = other.database;
}

NUCLEOTIDE::NUCLEOTIDE(
		const SHORT newIndex, 
		const char newLetter, 
		const std::string newName, 
		const bool newTypeIsPurine, 
		const SHORT newPairInDNAIndex, 
		const SHORT newPairINRNAIndex, 
		DATABASE* newDatabase)
{
	index = newIndex;

	letter = newLetter;
	name = std::string(newName);

	typeIsPurine = newTypeIsPurine;
	
	pairInDNAIndex = newPairInDNAIndex;
	pairInRNAIndex = newPairINRNAIndex;

	database = newDatabase;
}


bool NUCLEOTIDE::TypeIsPurine()
{
	return typeIsPurine;
}


SHORT NUCLEOTIDE::GetPairInDNAIndex()
{
	return pairInDNAIndex;
}

NUCLEOTIDE& NUCLEOTIDE::GetPairInDNA()
{
	return database->nucleotidesData[pairInDNAIndex];
}


SHORT NUCLEOTIDE::GetPairInRNAIndex()
{
	return pairInRNAIndex;
}

NUCLEOTIDE& NUCLEOTIDE::GetPairInRNA()
{
	return database->nucleotidesData[pairInRNAIndex];
}



NUCLEOTIDE::~NUCLEOTIDE()
{
}