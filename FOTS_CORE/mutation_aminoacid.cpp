#include "aminoacid.h"
#include "mutation_aminoacid.h"
#include "mutation_codon.h"
#include "database.h"


MUTATION_AMINOACID::MUTATION_AMINOACID()
{
	acid1Index = 0;
	acid2Index = 0;

	codonPosition = 0;

	bestMutationIndex = 0;

	database = NULL;
}

MUTATION_AMINOACID::MUTATION_AMINOACID(
		const MUTATION_AMINOACID& other)
{
	acid1Index = other.acid1Index;
	acid2Index = other.acid2Index;

	codonPosition = other.codonPosition;

	bestMutationIndex = other.bestMutationIndex;

	variableMutations = other.variableMutations;

	database = other.database;
}

MUTATION_AMINOACID::MUTATION_AMINOACID(
		const SHORT newAcid1Index, 
		const SHORT newAcid2Index, 
		const unsigned int newCodonPosition,
		const unsigned int peptideLength,
		DATABASE* newDatabase)
{
	database = newDatabase;

	acid1Index = newAcid1Index;
	acid2Index = newAcid2Index;

	codonPosition = newCodonPosition;

	bestMutationIndex = 0;
	double bestMutationDifficulty = 99.9;


	// ����������� ��������� ������� �������
	for(unsigned i = 0; i < database->acidsData[acid1Index].GetCodonsNumber(); i++)
		for(unsigned j = 0; j < database->acidsData[acid2Index].GetCodonsNumber(); j++)
		{
			SHORT index1 = database->acidsData[acid1Index].GetCodonIndex(i), index2 = database->acidsData[acid2Index].GetCodonIndex(j);

			// ������� ������������� ����������� �������� �������
			if(index1 == index2)
				continue;

			variableMutations.push_back(MUTATION_CODON(index1, index2, codonPosition, peptideLength, database));


			// ����� �������� �� "�����������" ����� �������
			if(variableMutations[variableMutations.size() - 1].GetDifficulty() < bestMutationDifficulty)
			{
				bestMutationIndex = variableMutations.size() - 1;
				bestMutationDifficulty = variableMutations[bestMutationIndex].GetDifficulty();
			}
		}
}

MUTATION_AMINOACID& MUTATION_AMINOACID::operator=(
		const MUTATION_AMINOACID& other)
{
	acid1Index = other.acid1Index;
	acid2Index = other.acid2Index;

	codonPosition = other.codonPosition;

	bestMutationIndex = other.bestMutationIndex;

	variableMutations = other.variableMutations;


	return *this;
}



std::string MUTATION_AMINOACID::GetProcess()
{
	return variableMutations[bestMutationIndex].GetProcess();
}

MUTATION_CODON& MUTATION_AMINOACID::GetBestMutation()
{
	return variableMutations[bestMutationIndex];
}

MUTATION_CODON& MUTATION_AMINOACID::GetMutation(
		const SHORT index)
{
	return variableMutations[index];
}

SHORT MUTATION_AMINOACID::GetMutationsNumber()
{
	return variableMutations.size();
}


void MUTATION_AMINOACID::Show()
{
	std::cout << "AMINOACID #1: " << (std::string)database->acidsData[acid1Index] << "\n";
	std::cout << "AMINOACID #2: " << (std::string)database->acidsData[acid2Index] << "\n\n";
	std::cout << "BEST MUTATION:\n\n";
	if(variableMutations.empty())
		std::cout << "THERE IS NO MUTATION\n";
	else
		variableMutations[bestMutationIndex].Show();
}


MUTATION_AMINOACID::~MUTATION_AMINOACID()
{
}
