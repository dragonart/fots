#pragma once
#include "fots_core.h"
#include "database.h"


class MUTATION_PEPTIDE
{
	public:
		MUTATION_PEPTIDE();
		MUTATION_PEPTIDE(
				const MUTATION_PEPTIDE& other);
		MUTATION_PEPTIDE(
				const LONG peptide1Index,
				const LONG peptide2Index,
				DATABASE* database);
		MUTATION_PEPTIDE& operator=(
				const MUTATION_PEPTIDE& other);


		std::string GetProcess();

		double GetDifficulty();

		unsigned int GetTransitionsNumber();
		unsigned int GetTransversionsNumber();

		unsigned int GetAcidMutationsNumber();
		unsigned int GetGroupChangesNumber();

		void Show();
		void SaveToFile(
				const std::string fileName);

		double CalculateEqualAcidsPart();
		double CalculateSimilarityGrantsam();

		bool operator==(
				const MUTATION_PEPTIDE& other);


		~MUTATION_PEPTIDE();


	private:
		LONG peptide1Index;
		LONG peptide2Index;

		std::string process;
		double similarity;

		double difficulty;
		
		unsigned int transitionsNumber;
		unsigned int transversionsNumber;
		
		unsigned int groupChangesNumber;

		unsigned int spacesNumber;

		std::string line1;
		std::string line2;

		LONG length;

		std::vector <MUTATION_AMINOACID> acidsMutations;
		
		DATABASE* database;
};