#pragma once
#include <vector>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>


typedef unsigned short SHORT; 
typedef unsigned long LONG;


#define CHAR_ZERO	48

#define TRANSITION_DIFFICULTY	1.0
#define TRANSVERSION_DIFFICULTY	2.2

#define NUCLEOTIDES_NUMBER			6
#define NUCLEOTIDES_IN_CODON_NUMBER	3

#define CODONS_NUMBER		64

#define AMINOACIDS_NUMBER	22

#define AMINOACIDGROUPS_NUMBER	5


#define PEPTIDE_MAX_LENGTH	99
#define DNA_MAX_LENGTH		999


#define ACID_GAPCOST		-1.0
#define NUCLEOTIDE_GAPCOST	-5

#define ALIGN_PEPTIDES	0
#define ALIGN_DNAS		1


	
#define NUCLEOTIDE_URIDINE	1
#define NUCLEOTIDE_CYTOSINE	2
#define NUCLEOTIDE_ADENINE	3
#define NUCLEOTIDE_GUANINE	4
#define NUCLEOTIDE_TIMINE	5



#define DATABASE_FIRSTLOAD	true
#define DATABASE_ADDNEW		false


#define LETTER_WRONG '@'


#define MAGIC_NUCLEAR_LENGTH 4

#define MIN(x, y) (x < y ? x : y)


std::string EnterSpacesForGoodNumberReading(
		LONG number,
		LONG max);

bool LinesAreSimilar(
		const std::string line1, 
		const std::string line2);

bool LinesAreEqual(
		const std::string line1,
		const std::string line2);

std::string NumberToString(
		LONG number);

std::string DoubleToString(
		const double number);

double Max(
		const double x,
		const double y,
		const double z);