#pragma once
#include "fots_core.h"
#include "database.h"


class MUTATION_AMINOACID
{
	public:
		MUTATION_AMINOACID();
		MUTATION_AMINOACID(
				const MUTATION_AMINOACID& other);
		MUTATION_AMINOACID(
				const SHORT acid1Index,
				const SHORT acid2Index,
				const unsigned int codonPosition,
				const unsigned int peptideLength,
				DATABASE* database);

		MUTATION_AMINOACID& operator=(
				const MUTATION_AMINOACID& other);
		

		std::string GetProcess();

		MUTATION_CODON& GetBestMutation();
		MUTATION_CODON& GetMutation(
				const SHORT index);

		SHORT GetMutationsNumber();

		void Show();


		~MUTATION_AMINOACID();
		

	private:
		SHORT acid1Index;
		SHORT acid2Index;

		unsigned int codonPosition;

		SHORT bestMutationIndex;

		std::vector <MUTATION_CODON> variableMutations;

		DATABASE* database;
};