#include "peptideclass.h"


PEPTIDECLASS::PEPTIDECLASS()
{
	index = 0;
	name = "";

	database = NULL;
}
	
PEPTIDECLASS::PEPTIDECLASS(
		const PEPTIDECLASS& other)
{
	index = other.index;
	name = other.name;
	dnasIndices = other.dnasIndices;

	database = other.database;
}

PEPTIDECLASS::PEPTIDECLASS(
		const LONG newIndex,
		const std::string newName,
		std::vector <LONG> newDNAsIndices,
		DATABASE* newDatabase)
{
	index = newIndex;
	name = std::string(newName);
	dnasIndices = newDNAsIndices;

	database = newDatabase;
}


	
LONG PEPTIDECLASS::GetIndex()
{
	return index;
}
		
std::string PEPTIDECLASS::GetName()
{
	return name;
}
		
		
LONG PEPTIDECLASS::GetDNAsNumber()
{
	return dnasIndices.size();
}

		
DNA& PEPTIDECLASS::operator[](
		LONG indexInClass)
{
	return GetDNA(indexInClass);
}
	
DNA& PEPTIDECLASS::GetDNA(
		LONG indexInClass)
{
	return database->dnasData[dnasIndices[indexInClass]];
}


LONG PEPTIDECLASS::GetDNAIndex(
		LONG indexInClass)
{
	return dnasIndices[indexInClass];
}

		

PEPTIDECLASS::~PEPTIDECLASS()
{
}