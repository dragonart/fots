#pragma once
#include "fots_core.h"
#include "database.h"


class PEPTIDE
{
	public:
		PEPTIDE();
		PEPTIDE(
				const PEPTIDE& other);
		PEPTIDE(
				const LONG index,
				const LONG dnaIndex,
				const std::string name,
				const std::string line,
				DATABASE* database);


		LONG GetDNAIndex();
		DNA& GetDNA();
		
		std::string GetName();
		
		AMINOACID& operator[](
				const unsigned int positionInPeptide);
		AMINOACID& GetAcid(
				const unsigned int positionInPeptide);
		
		SHORT GetAcidIndex(
				const unsigned int positionInPeptide);
			
		unsigned int GetAcidNumber(
				const SHORT acidIndex);
		double GetAcidStake(
				const SHORT acidIndex);
		
		LONG GetLength();

		operator LONG() {return index;}
		operator std::string() {return line;}


		~PEPTIDE();


	private:
		LONG index;
		LONG dnaIndex;
		
		std::string name;
		std::string line;
		
		std::vector <SHORT> acidsIndices;

		DATABASE* database;
};