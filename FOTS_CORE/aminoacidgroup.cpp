#include "aminoacidgroup.h"
#include "database.h"
#include "aminoacid.h"


AMINOACIDGROUP::AMINOACIDGROUP()
{
	index = 0;	
	name = "empty group";

	database = NULL;
}

AMINOACIDGROUP::AMINOACIDGROUP(
		const AMINOACIDGROUP& other)
{
	index = other.index;	
	name = other.name;	
	acidsIndices = other.acidsIndices;

	database = other.database;
}

AMINOACIDGROUP::AMINOACIDGROUP(
		const SHORT newIndex, 
		const std::string newName, 
		std::vector <SHORT> newAcidsIndices,
		DATABASE* newDatabase)
{
	index = newIndex;
	name = std::string(newName);
	acidsIndices = newAcidsIndices;

	database = newDatabase;
}



AMINOACID& AMINOACIDGROUP::operator[](
		const SHORT acidIndexInGroup)
{
	return GetAcid(acidIndexInGroup);
}

AMINOACID& AMINOACIDGROUP::GetAcid(
		const SHORT acidIndexInGroup)
{
	return database->acidsData[acidsIndices[acidIndexInGroup]];
}


SHORT AMINOACIDGROUP::GetAcidIndex(
		const SHORT acidIndexInGroup)
{
	return acidsIndices[acidIndexInGroup];
}


SHORT AMINOACIDGROUP::GetAcidsNumber()
{
	return acidsIndices.size();
}

SHORT AMINOACIDGROUP::GetCodonsNumber()
{
	SHORT result = 0;

	for(SHORT i = 0; i < acidsIndices.size(); i++)
		result += database->acidsData[acidsIndices[i]].GetCodonsNumber();

	return result;
}



AMINOACIDGROUP::~AMINOACIDGROUP()
{
}