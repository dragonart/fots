#include "codon.h"
#include "aminoacid.h"
#include "database.h"
#include "mutation_codon.h"
#include "mutation_nucleotide.h"


MUTATION_CODON::MUTATION_CODON()
{
	codon1Index = 0;
	codon2Index = 0;

	process = std::string("");
	
	codonPosition = 0;

	difficulty = 0.0;

	for(SHORT i = 0; i < NUCLEOTIDES_IN_CODON_NUMBER; i++)
		positionsChanged[i] = false;

	transitionsNumber = 0;
	transversionsNumber = 0;

	database = NULL;
}

MUTATION_CODON::MUTATION_CODON(
		const MUTATION_CODON& other)
{
	codon1Index = other.codon1Index;
	codon2Index = other.codon2Index;
	
	process = other.process;

	codonPosition = other.codonPosition;

	difficulty = other.difficulty;
	
	for(SHORT i = 0; i < NUCLEOTIDES_IN_CODON_NUMBER; i++)
		positionsChanged[i] = other.positionsChanged[i];

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	database = other.database;
}

MUTATION_CODON::MUTATION_CODON(
		const SHORT newCodon1Index, 
		const SHORT newCodon2Index, 
		const unsigned int newCodonPosition,
		const unsigned int peptideLength,
		DATABASE* newDatabase)
{
	database = newDatabase;

	codon1Index = newCodon1Index;
	codon2Index = newCodon2Index;

	codonPosition = newCodonPosition;


	// ��������� ����� ����������

	difficulty = 0.0;

	/*
		������ �������� �������:
		[�������_����������_�������_�������_��_����������_����� �_�������_������] �������1, �������2...   <--- ���������������_�����_��_�������_���������

		�������:
		[21] A->G[1]  C->A[2]  G->T[3]   <--- 73.2% A->Q
		[66]          T->A[2]            <--- 92.1% D->S
		[ 1] C->T[1]           A->G[3]   <--- 61.9% L->W
																																								*/

	process = std::string("");
	process += "[" + EnterSpacesForGoodNumberReading(codonPosition, peptideLength);
	process += NumberToString(codonPosition);
	process += "] ";

	transitionsNumber = 0;
	transversionsNumber = 0;


	for(short i = 0; i < NUCLEOTIDES_IN_CODON_NUMBER; i++)
	{
		SHORT nucleotideFromCodon1Index = database->codonsData[codon1Index].GetNucleotideIndexAtPosition(i);
		SHORT nucleotideFromCodon2Index = database->codonsData[codon2Index].GetNucleotideIndexAtPosition(i);
		
		// ���������� �������� ����������� �������� ���������� ����� �������
		if(nucleotideFromCodon1Index == nucleotideFromCodon2Index)
		{
			process += "          ";
			positionsChanged[i] = false;
		}
		else
		{
			// ����� ������ �������
			MUTATION_NUCLEOTIDE currentNucleotideMutation(nucleotideFromCodon1Index, nucleotideFromCodon2Index, i + 1, database);

			difficulty += currentNucleotideMutation.GetDifficulty();
			process += currentNucleotideMutation.GetProcess() + "   ";

			transitionsNumber += currentNucleotideMutation.MutationIsTransition();
			transversionsNumber += (!currentNucleotideMutation.MutationIsTransition());

			positionsChanged[i] = true;
		}
	}

	process += "<--- " + DoubleToString(CalculateGrantsamImportance() * 100.0) + "% ";
	
	process += (char)database->codonsData[codon1Index].GetAcid();
	process += "->";
	process += (char)database->codonsData[codon2Index].GetAcid();
}

MUTATION_CODON& MUTATION_CODON::operator=(
	const MUTATION_CODON& other)
{
	database = other.database;

	codon1Index = other.codon1Index;
	codon2Index = other.codon2Index;
	
	process = other.process;

	codonPosition = other.codonPosition;

	difficulty = other.difficulty;
	
	for(SHORT i = 0; i < NUCLEOTIDES_IN_CODON_NUMBER; i++)
		positionsChanged[i] = other.positionsChanged[i];

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	return *this;
}



std::string MUTATION_CODON::GetProcess()
{
	return process;
}

double MUTATION_CODON::GetDifficulty()
{
	return difficulty;
}

SHORT MUTATION_CODON::GetTransitionsNumber()
{
	return transitionsNumber;
}

SHORT MUTATION_CODON::GetTransversionsNumber()
{
	return transversionsNumber;
}


bool MUTATION_CODON::MutationChangesAcid()
{
	bool result;

	// �������� ����������� ��������������� �����������
	result = (database->codonsData[codon1Index].GetAcidIndex() != database->codonsData[codon2Index].GetAcidIndex());

	return result;
}

bool MUTATION_CODON::MutationChangesAcidGroup()
{
	bool result;

	// ����� ������ �������� ����������� �������� ����� �����������, ������� ������������� ������
	result = (database->acidsData[database->codonsData[codon1Index].GetAcidIndex()].GetGroupIndex() != database->acidsData[database->codonsData[codon2Index].GetAcidIndex()].GetGroupIndex());

	return result;
}


void MUTATION_CODON::Show()
{
	std::cout << "CODON #1: " << (std::string)database->codonsData[codon1Index] << "\n";
	std::cout << "CODON #2: " << (std::string)database->codonsData[codon2Index] << "\n";
	if(codon1Index == codon2Index)
		std::cout << "THERE IS NO MUTATION\n";
	else
	{
		std::cout << "PROCESS: " << process << "\n";
		std::cout << "TRANSITIONS: " << transitionsNumber << "\n";
		std::cout << "TRANSVERSIONS: " << transversionsNumber << "\n";
		std::cout << "DIFFICULTY: " << difficulty << "\n";
		if(MutationChangesAcid())
		{
			std::cout << "MUTATION CHANGES ACID: from " << (std::string)database->acidsData[database->codonsData[codon1Index].GetAcidIndex()] << " to " << (std::string)database->acidsData[database->codonsData[codon2Index].GetAcidIndex()] << "\n";
			if(MutationChangesAcidGroup())
				std::cout << "MUTATION CHANGES ACID GROUP\n";
			else
				std::cout << "MUTATION DON'T CHANGES ACID GROUP\n";
		}
		else
			std::cout << "MUTATION DON'T CHANGES ACID (" << (std::string)database->acidsData[database->codonsData[codon1Index].GetAcidIndex()] << ")\n"; 
	}
}


double MUTATION_CODON::CalculateGrantsamImportance()
{
	return database->grantsamTable.GetSimilarity(database->codonsData[codon1Index].GetAcidIndex(), database->codonsData[codon2Index].GetAcidIndex());
}


MUTATION_CODON::~MUTATION_CODON()
{
}
