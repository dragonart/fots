#include "codon.h"
#include "database.h"
#include "nucleotide.h"


CODON::CODON()
{
	index = 0;
	acidIndex = 0;

	database = NULL;
}

CODON::CODON(
		const CODON& other)
{
	index = other.index;
	acidIndex = other.acidIndex;
	nucleotidesIndices = other.nucleotidesIndices;

	database = other.database;
}

CODON::CODON(
		const SHORT newIndex, 
		const SHORT newAcidIndex, 
		std::vector <SHORT> newNucleotidesIndices, 
		DATABASE* newDatabase)
{	
	index = newIndex;
	acidIndex = newAcidIndex;
	nucleotidesIndices = newNucleotidesIndices;

	database = newDatabase;
}



SHORT CODON::GetAcidIndex()
{
	return acidIndex;
}

AMINOACID& CODON::GetAcid()
{
	return database->acidsData[acidIndex];
}


NUCLEOTIDE& CODON::operator[](
		const SHORT positionIndex)
{
	return GetNucleotideAtPosition(positionIndex);
}

NUCLEOTIDE& CODON::GetNucleotideAtPosition(
		const SHORT position)
{
	return database->nucleotidesData[nucleotidesIndices[position]];
}


SHORT CODON::GetNucleotideIndexAtPosition(
		const SHORT positionIndex)
{	
	return nucleotidesIndices[positionIndex];
}

		
std::string CODON::CodonToString()
{
	/*
		������ ������:
		��� ����� �����������

		������:
		AUG
								*/
	std::string result("");

	for(SHORT i = 0; i < NUCLEOTIDES_IN_CODON_NUMBER; i++)
		result += (char)database->nucleotidesData[nucleotidesIndices[i]];

	return result;
}



CODON::~CODON()
{
}