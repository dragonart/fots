#include "rna.h"
#include "database.h"


RNA::RNA()
{
	index = 0;
	dnaIndex = 0;

	name = "";
	line = "";

	database = NULL;
}

RNA::RNA(
		const RNA& other)
{
	index = other.index;
	dnaIndex = other.dnaIndex;
	
	name = other.name;
	line = other.line;

	database = other.database;
}

RNA::RNA(
		const LONG newIndex,
		const LONG newDNAIndex,
		const std::string newName, 
		const std::string newLine,
		DATABASE* newDatabase)
{
	index = newIndex;
	dnaIndex = newDNAIndex;
	
	name = newName;
	line = newLine;

	database = newDatabase;
}



LONG RNA::GetIndex()
{
	return index;
}

LONG RNA::GetDNAIndex()
{
	return dnaIndex;
}


std::string RNA::GetName()
{
	return name;
}
		
std::string RNA::GetLine()
{
	return line;
}
		

NUCLEOTIDE& RNA::operator[](const unsigned int position)
{
	return GetNucleotideAtPosition(position);
}

NUCLEOTIDE& RNA::GetNucleotideAtPosition(const unsigned int position)
{
	return database->nucleotidesData[nucleotidesIndices[position]];
}


SHORT RNA::GetNucleotideIndexAtPosition(
		const unsigned int position)
{
	return nucleotidesIndices[position];
}


DNA& RNA::GetDNA()
{
	return database->dnasData[dnaIndex];
}

int RNA::GetLength()
{
	return nucleotidesIndices.size();
}



RNA::~RNA()
{
}
