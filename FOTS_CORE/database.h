#pragma once
#include "fots_core.h"


class NUCLEOTIDE;
class CODON;
class AMINOACID;
class AMINOACIDGROUP;
class PEPTIDE;
class DNA;
class RNA;
class PEPTIDECLASS;

class MUTATION_NUCLEOTIDE;
class MUTATION_CODON;
class MUTATION_AMINOACID;
class MUTATION_PEPTIDE;
class MUTATION_DNA;


class DATABASE
{
	public:
		DATABASE();
		DATABASE(
				const DATABASE& other);
		DATABASE(
				const std::string fileNucleotides,
				const std::string fileCodons,
				const std::string fileAcids,
				const std::string fileGroups,
				const std::string filePeptides,
				const std::string fileDNAs,
				const std::string fileRNAs,
				const std::string filePeptideClasses,
				const std::string fileGrantsamTable);
		

		bool SaveAll();
		bool LoadAll();

		
		friend class DATABASE_NUCLEOTIDES;
		friend class DATABASE_CODONS;
		friend class DATABASE_AMINOACIDS;
		friend class DATABASE_AMINOACIDGROUPS;
		friend class DATABASE_PEPTIDES;
		friend class DATABASE_DNAS;
		friend class DATABASE_RNAS;
		friend class DATABASE_PEPTIDECLASSES;
		friend class GRANTSAMTABLE;
		friend class LINEALIGNER;

		friend class NUCLEOTIDE;
		friend class CODON;
		friend class AMINOACID;
		friend class AMINOACIDGROUP;
		friend class PEPTIDE;
		friend class PEPTIDECLASS;
		friend class DNA;
		friend class RNA;

		friend class MUTATION_NUCLEOTIDE;
		friend class MUTATION_CODON;
		friend class MUTATION_AMINOACID;
		friend class MUTATION_PEPTIDE;
		friend class MUTATION_DNA;

		
		~DATABASE();


		class DATABASE_NUCLEOTIDES
		{
			public:
				DATABASE_NUCLEOTIDES();
				DATABASE_NUCLEOTIDES(
						const DATABASE_NUCLEOTIDES& other);
				DATABASE_NUCLEOTIDES(
						const std::string fileName,
						DATABASE* database);

				DATABASE_NUCLEOTIDES& operator=(
						const DATABASE_NUCLEOTIDES& other);


				bool Save();
				bool Load();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();
				
				NUCLEOTIDE& operator[](
						const SHORT index);
				NUCLEOTIDE& GetNucleotideFromIndex(
						const SHORT index);

				NUCLEOTIDE& GetNucleotideFromLetter(
						const char letter);
				
				NUCLEOTIDE& GetPairInDNA(
						const SHORT nucleotideIndex);
				SHORT GetPairIndexInDNA(
						const SHORT nucleotideIndex);

				NUCLEOTIDE& GetPairInRNA(
						const SHORT nucleotideIndex);
				SHORT GetPairIndexInRNA(
						const SHORT nucleotideIndex);
				
				MUTATION_NUCLEOTIDE DetermineNucleotideMutation(
						const SHORT nucleotide1Index,
						const SHORT nucleotide2Index,
						const LONG position);

				void ShowNucleotideFromLetter(
						const char letter);
				void ShowNucleotideFromIndex(
						const SHORT index);
				
				int DATABASE::DATABASE_NUCLEOTIDES::GetChangingCost(
						const SHORT nucleotide1Index,
						const SHORT nucleotide2Index);

				
				~DATABASE_NUCLEOTIDES();


			private:
				std::string fileName;
				
				std::vector <NUCLEOTIDE> nucleotides;
				std::vector <std::vector <int>> tableCosts;

				DATABASE* database;
		};


		class DATABASE_CODONS
		{

			public:
				DATABASE_CODONS();
				DATABASE_CODONS(
						const DATABASE_CODONS& other);
				DATABASE_CODONS(
						const std::string fileName,
						DATABASE* database);

				DATABASE_CODONS& operator=(
						const DATABASE_CODONS& other);


				bool SaveToFile(
						const std::string fileName);
				bool Load();
				bool Save();

				void ShowData();

				CODON& operator[](
						SHORT index);

				MUTATION_CODON DetermineCodonMutation(
						const SHORT codon1Index,
						const SHORT codon2Index,
						const unsigned int position);

				void ShowCodonFromIndex(
						SHORT index);
				void ShowCodonFrom3Letters(
						const char letter1, 
						const char letter2, 
						const char letter3);

				CODON& GetCodonFrom3Letters(
						const char letter1, 
						const char letter2, 
						const char letter3);

				~DATABASE_CODONS();


			private:
				std::string fileName;

				std::vector <CODON> codons;	

				DATABASE* database;
		};	


		class DATABASE_AMINOACIDS
		{
			public:
				DATABASE_AMINOACIDS();
				DATABASE_AMINOACIDS(
						const DATABASE_AMINOACIDS& other);
				DATABASE_AMINOACIDS(
						const std::string fileName,
						DATABASE* database);

				DATABASE_AMINOACIDS& operator=(
						const DATABASE_AMINOACIDS& other);


				bool Save();
				bool Load();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();
				
				AMINOACID& operator[](
						SHORT index);
				
				AMINOACID& GetAcidFromLetter(
						const char letter);
				
				MUTATION_AMINOACID DetermineAcidMutation(
						const SHORT acid1Index,
						const SHORT acid2Index,
						const unsigned int position);

				SHORT GetAcidsNumber();

				void ShowAcidFromIndex(
						const SHORT index);
				void ShowAcidFromLetter(
						const char letter);
								

				~DATABASE_AMINOACIDS();


			private:
				std::string fileName;

				std::vector <AMINOACID> acids;	

				DATABASE* database;
		};


		class DATABASE_AMINOACIDGROUPS
		{
			public:
				DATABASE_AMINOACIDGROUPS();
				DATABASE_AMINOACIDGROUPS(
						const DATABASE_AMINOACIDGROUPS& other);
				DATABASE_AMINOACIDGROUPS(
						const std::string fileName,
						DATABASE* database);

				DATABASE_AMINOACIDGROUPS& operator=(
						const DATABASE_AMINOACIDGROUPS& other);


				bool Save();
				bool Load();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();

				AMINOACIDGROUP& operator[](
						const SHORT index);

				void ShowGroupFromIndex(
						const SHORT index);
				

				~DATABASE_AMINOACIDGROUPS();


			private:
				std::string fileName;

				std::vector <AMINOACIDGROUP> groups;

				DATABASE* database;
		};


		class DATABASE_PEPTIDES
		{
			public:
				DATABASE_PEPTIDES();
				DATABASE_PEPTIDES(
						const DATABASE_PEPTIDES& other);
				DATABASE_PEPTIDES(
						const std::string fileName,
						DATABASE* database);

				DATABASE_PEPTIDES& operator=(
						const DATABASE_PEPTIDES& other);


				bool Load();
				bool Save();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();
				void ShowPeptideFromIndex(
						LONG index);
				void ShowPeptideFromName(
						const std::string name);

				bool Add(
						PEPTIDE newPeptide);
				bool AddFromKeyboard();
				bool AddFromFile(
						const std::string fileName,
						bool firstLoad);

				PEPTIDE& operator[](
						const LONG index);

				LONG GetTotalAcidsNumber(
						const SHORT acidIndex);
				double GetTotalAcidsStake(
						const SHORT acidIndex);
				
				MUTATION_PEPTIDE DeterminePeptideMutation(
						const LONG peptide1Index,
						const LONG peptide2Index);
				MUTATION_PEPTIDE GetFakeMutation();


				PEPTIDE& GetPeptideFromName(
						const std::string name);

				LONG GetSize();

				std::vector <std::string> SearchPeptideNames(
						const std::string searchingName);
				PEPTIDE& SearchPeptidesAndChooseOne(
						const std::string line);
				
				
				~DATABASE_PEPTIDES();


			private:
				std::string fileName;

				std::vector <PEPTIDE> peptides;
				
				LONG totalMutationsNumber;
				LONG totalTransitionsNumber;
				LONG totalTransversionsNumber;
				LONG totalGroupChangesNumber;

				std::vector <LONG> totalAcidsNumbers;
				std::vector <LONG> totalPositionChangesNumbers;

				DATABASE* database;
		};


		class DATABASE_DNAS
		{
			public:
				DATABASE_DNAS();
				DATABASE_DNAS(
						const DATABASE_DNAS& other);
				DATABASE_DNAS(
						const std::string fileName,
						DATABASE* database);

				DATABASE_DNAS& operator=(
						const DATABASE_DNAS& other);


				bool Load();
				bool Save();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();
				void ShowDNAFromIndex(
						const LONG index);
				void ShowDNAFromName(
						const std::string name);

				bool Add(
						const DNA dna);
				bool AddFromKeyboard();
				bool AddFromFile(
						const std::string fileName,
						bool firstLoad);
				
				DNA& operator[](
						const LONG index);
				
				MUTATION_DNA DetermineDNAMutation(
						const LONG dna1Index,
						const LONG dna2Index);
				MUTATION_DNA GetFakeMutation();

				long GetTotalNucleotideNumber(
						const short nucleotideIndex);

				DNA& GetDNAFromName(
						const std::string name);

				LONG GetSize();

				std::vector <std::string> SearchDNANames(
						const std::string searchingName);
				DNA& SearchDNAsAndChooseOne(
						const std::string line);
		

				~DATABASE_DNAS();


			private:
				std::string fileName;

				std::vector <DNA> dnas;

				std::vector <LONG> totalNucleotidesNumbers;

				DATABASE* database;
		};


		class DATABASE_RNAS
		{
			public:
				DATABASE_RNAS();
				DATABASE_RNAS(
						const DATABASE_RNAS& other);
				DATABASE_RNAS(
						const std::string fileName,
						DATABASE* database);

				DATABASE_RNAS& operator=(
						const DATABASE_RNAS& other);


				bool Load();
				bool Save();
				bool SaveToFile(
					const std::string fileName);
		
				void ShowData();
				
				bool Add(
						const RNA rna);
				bool AddFromKeyboard();
				bool AddFromFile(
						const std::string fileName);
		
				RNA& operator[](
						const LONG index);

				long GetTotalNucleotideNumber(
						const short nucleotideIndex);


				~DATABASE_RNAS();


			private:
				std::string fileName;

				std::vector <RNA> rnas;

				std::vector <LONG> totalNucleotidesNumbers;

				DATABASE* database;
		};
		

		class DATABASE_PEPTIDECLASSES
		{
			public:
				DATABASE_PEPTIDECLASSES();
				DATABASE_PEPTIDECLASSES(
						const DATABASE_PEPTIDECLASSES& other);
				DATABASE_PEPTIDECLASSES(
						const std::string fileName,
						DATABASE* database);

				DATABASE_PEPTIDECLASSES& operator=(
						const DATABASE_PEPTIDECLASSES& other);


				bool Save();
				bool Load();
				bool SaveToFile(
						const std::string fileName);

				void ShowData();

				PEPTIDECLASS& operator[](
						const LONG index);
				

				~DATABASE_PEPTIDECLASSES();


			private:
				std::string fileName;

				std::vector <PEPTIDECLASS> classes;

				DATABASE* database;
		};


		class GRANTSAMTABLE
		{
			public:
				GRANTSAMTABLE();
				GRANTSAMTABLE(
						const GRANTSAMTABLE& other);
				GRANTSAMTABLE(
						const std::string fileName,
						DATABASE* database);

				GRANTSAMTABLE& operator=(
						const GRANTSAMTABLE& other);


				bool Load();
				bool Save();
				bool SaveToFile(
						const std::string fileName);

				double GetSimilarity(
						SHORT acid1Index, 
						SHORT acid2Index);

				bool MutationIsConservative(
						SHORT acid1Index, 
						SHORT acid2Index);

				void ShowData();


				~GRANTSAMTABLE();


			private:
				std::string fileName;

				std::vector <std::vector <double>> table;

				DATABASE* database;
		};


		class LINEALIGNER
		{
			public:
				LINEALIGNER();
				LINEALIGNER(
						const LINEALIGNER& other);
				LINEALIGNER(	
						DATABASE* database);


				std::vector <std::string> AlignPeptides(
						const LONG peptide1Index,
						const LONG peptide2Index);
				std::vector <std::string> AlignDNAs(
						const LONG dna1Index,
						const LONG dna2Index);

				~LINEALIGNER();

				
			private:
				DATABASE* database;
		};



		DATABASE_NUCLEOTIDES& GetNucleotidesData();
		DATABASE_CODONS& GetCodonsData();
		DATABASE_AMINOACIDS& GetAcidsData();
		DATABASE_AMINOACIDGROUPS& GetGroupsData();
		DATABASE_PEPTIDES& GetPeptidesData();
		DATABASE_DNAS& GetDNAsData();
		DATABASE_RNAS& GetRNAsData();
		DATABASE_PEPTIDECLASSES& GetPeptideClassesData();
		GRANTSAMTABLE& GetGrantsamTable();

		std::vector <std::string> AlignLines(
				const SHORT alignObjectType,
				const LONG index1,
				const LONG index2);


	private:
		DATABASE_NUCLEOTIDES nucleotidesData;
		DATABASE_CODONS codonsData;
		DATABASE_AMINOACIDS acidsData;
		DATABASE_AMINOACIDGROUPS groupsData;
		DATABASE_PEPTIDES peptidesData;
		DATABASE_DNAS dnasData;
		DATABASE_RNAS rnasData;
		DATABASE_PEPTIDECLASSES peptideClassesData;
		GRANTSAMTABLE grantsamTable;
		LINEALIGNER aligner;
};