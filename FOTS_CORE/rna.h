#pragma once
#include "fots_core.h"
#include "database.h"


class RNA
{
	public:
		RNA();
		RNA(
				const RNA& other);
		RNA(
				const LONG index,
				const LONG dnaIndex,
				const std::string name,
				const std::string line,
				DATABASE* database);


		LONG GetIndex();

		std::string GetName();
		std::string GetLine();
		
		NUCLEOTIDE& operator[](
				const unsigned int position);
		NUCLEOTIDE& GetNucleotideAtPosition(
				const unsigned int position);

		SHORT GetNucleotideIndexAtPosition(
				const unsigned int position);
		
		DNA& GetDNA();
		LONG GetDNAIndex();

		int GetLength();


		~RNA();


	private:
		LONG index;
		LONG dnaIndex;
		
		std::string name;
		std::string line;

		std::vector <SHORT> nucleotidesIndices;		

		DATABASE* database;
};