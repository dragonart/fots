#pragma once
#include "fots_core.h"
#include "database.h"


class DNA
{
	public:
		DNA();
		DNA(
				const DNA& other);
		DNA(
				const LONG index,
				const LONG rnaIndex,
				const LONG peptideIndex,
				const LONG peptideClassIndex,
				const std::string name,
				const std::string line,
				DATABASE* database);


		std::string GetName();

		NUCLEOTIDE& operator[](
				const unsigned int position);
		NUCLEOTIDE& GetNucleotideAtPosition(
				const unsigned int position);
		
		SHORT GetNucleotideIndexAtPosition(
				const unsigned int position);

		RNA& GetRNA();
		LONG GetRNAIndex();

		PEPTIDE& GetPeptide();
		LONG GetPeptideIndex();

		PEPTIDECLASS& GetPeptideClass();
		LONG GetPeptideClassIndex();

		double CalculateBeloserskyCoefficient();

		unsigned int GetLength();

		operator LONG() {return index;}
		operator std::string() {return line;}


		~DNA();


	private:
		LONG index;
		LONG rnaIndex;
		LONG peptideIndex;
		LONG peptideClassIndex;
	
		
		std::string name;
		std::string line;

		std::vector <SHORT> nucleotidesIndices;

		DATABASE* database;
};

