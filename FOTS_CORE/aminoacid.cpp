#include "aminoacid.h"
#include "database.h"


AMINOACID::AMINOACID()
{
	index = 0;

	letter = '@';
	name = "empty acid";

	groupIndex = 0;

	database = NULL;
}

AMINOACID::AMINOACID(
		const AMINOACID& other)
{
	index = other.index;

	letter = other.letter;
	name = other.name;
		
	groupIndex = other.groupIndex;

	codonsIndices = other.codonsIndices;

	database = other.database;
}

AMINOACID::AMINOACID(
		const SHORT newIndex, 
		const char newLetter, 
		const std::string newName, 
		const SHORT newGroupIndex, 
		std::vector <SHORT> newCodonsIndices,
		DATABASE* newDatabase)
{
	index = newIndex;

	letter = newLetter;
	name = newName;

	groupIndex = newGroupIndex;
	
	codonsIndices = newCodonsIndices;

	database = newDatabase;
}



SHORT AMINOACID::GetGroupIndex()
{
	return groupIndex;
}

AMINOACIDGROUP& AMINOACID::GetGroup()
{
	return database->groupsData[groupIndex];
}


CODON& AMINOACID::operator[](
		const SHORT index)
{
	return GetCodon(index);
}

CODON& AMINOACID::GetCodon(
		const SHORT index)
{
	return database->codonsData[codonsIndices[index]];
}


SHORT AMINOACID::GetCodonIndex(
		const SHORT index)
{
	return codonsIndices[index];
}


SHORT AMINOACID::GetCodonsNumber()
{
	return codonsIndices.size();
}



AMINOACID::~AMINOACID()
{
}