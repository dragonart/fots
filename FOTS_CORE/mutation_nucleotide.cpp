#include "mutation_nucleotide.h"
#include "database.h"
#include "nucleotide.h"


MUTATION_NUCLEOTIDE::MUTATION_NUCLEOTIDE()
{
	nucleotide1Index = 0;
	nucleotide2Index = 0;

	processForCodon = std::string("");
	processForDNA = std::string("");

	position = 0;

	difficulty = 0.0;

	mutationIsTransition = false;

	database = NULL;
}

MUTATION_NUCLEOTIDE::MUTATION_NUCLEOTIDE(
		const MUTATION_NUCLEOTIDE& other)
{
	nucleotide1Index = other.nucleotide1Index;
	nucleotide2Index = other.nucleotide2Index;

	processForCodon = other.processForCodon;
	processForDNA = other.processForDNA;

	position = other.position;

	difficulty = other.difficulty;

	mutationIsTransition = other.mutationIsTransition;

	database = other.database;
}


MUTATION_NUCLEOTIDE::MUTATION_NUCLEOTIDE(
		const SHORT newNucleotide1Index, 
		const SHORT newNucleotide2Index,
		const unsigned int newPosition, 
		DATABASE* newDatabase)
{
	database = newDatabase;

	nucleotide1Index = newNucleotide1Index;
	nucleotide2Index = newNucleotide2Index;

	position = newPosition;


	// ��������� ��������� ���������� ����������

	// ����������� ��������� �������: � ����������� �� ���� �������
	if(mutationIsTransition)
		difficulty = TRANSITION_DIFFICULTY;
	else
		difficulty = TRANSVERSION_DIFFICULTY;
	if(nucleotide1Index == nucleotide2Index)
		difficulty = 0.0;

	// �������� ��������� - ���������� ������ ���������� � ������ ���� (������ ��� ����������) -> �������� ����� ���� �����
	mutationIsTransition = (database->nucleotidesData.GetPairIndexInDNA(nucleotide1Index) == nucleotide2Index);

	/* 
		������ �������� �������:
		�����_�������_����������->�����_�������_����������[�_�������_�����������] 

		������:
		A->G[2]

		��� ������� ��� ��������:
		[�������_���_����������_������������� �_�������_����������] �����_�������_����������->�����_�������_����������
																														*/
	processForCodon = std::string("");
	processForCodon = (char)database->nucleotidesData[nucleotide1Index];
	processForCodon += "->";
	processForCodon += (char)database->nucleotidesData[nucleotide2Index];
	processForCodon += std::string("[");
	processForCodon += position + CHAR_ZERO;
	processForCodon += std::string("]");

	processForDNA = std::string("");
	processForDNA = "[";
	processForDNA += EnterSpacesForGoodNumberReading(position, DNA_MAX_LENGTH);
	processForDNA += NumberToString(position);
	processForDNA += "] ";
	processForDNA += (char)database->nucleotidesData[nucleotide1Index];
	processForDNA += "->";
	processForDNA += (char)database->nucleotidesData[nucleotide2Index];
}


MUTATION_NUCLEOTIDE& MUTATION_NUCLEOTIDE::operator=(
		const MUTATION_NUCLEOTIDE& other)
{
	database = other.database;

	nucleotide1Index = other.nucleotide1Index;
	nucleotide2Index = other.nucleotide2Index;

	processForCodon = other.processForCodon;
	processForDNA = other.processForDNA;

	position = other.position;

	difficulty = other.difficulty;

	mutationIsTransition = other.mutationIsTransition;

	return *this;
}



std::string MUTATION_NUCLEOTIDE::GetProcess(bool forCodon)
{
	if(forCodon)
		return processForCodon;
	else
		return processForDNA;
}


double MUTATION_NUCLEOTIDE::GetDifficulty()
{
	return difficulty;
}


bool MUTATION_NUCLEOTIDE::MutationIsTransition()
{
	return mutationIsTransition;
}

bool MUTATION_NUCLEOTIDE::TransversionIsFromPurineToPyrimidine()
{
	bool result;

	result = (database->nucleotidesData[nucleotide1Index].TypeIsPurine() && !database->nucleotidesData[nucleotide2Index].TypeIsPurine());

	return result;
}


void MUTATION_NUCLEOTIDE::Show()
{
	std::cout << "PROCESS: " << (char)database->nucleotidesData[nucleotide1Index] << "->" << (char)database->nucleotidesData[nucleotide2Index] << "\n";
	if(difficulty)
	{
		std::cout << "TYPE: " << (mutationIsTransition ? "transition" : "transversion") << "\n";
		if(!mutationIsTransition)
			if(TransversionIsFromPurineToPyrimidine())
				std::cout << "FROM PURINE TO PYRIMIDINE\n";
			else
				std::cout << "FROM PYRIMIDINE TO PURINE\n";
	}
	else
		std::cout << "THERE IS NO MUTATION\n";
}



MUTATION_NUCLEOTIDE::~MUTATION_NUCLEOTIDE()
{
}