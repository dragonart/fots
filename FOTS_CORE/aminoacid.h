#pragma once
#include "fots_core.h"
#include "database.h"


class AMINOACID
{
	public:
		AMINOACID();
		AMINOACID(
				const AMINOACID& other);
		AMINOACID(
				const SHORT index,
				const char letter,
				const std::string name,
				const SHORT groupIndex,
				std::vector <SHORT> codonsIndices,
				DATABASE* database);

		
		SHORT GetGroupIndex();
		AMINOACIDGROUP& GetGroup();
				
		CODON& operator[](
				const SHORT index);
		CODON& GetCodon(
				const SHORT index);

		SHORT GetCodonIndex(
				const SHORT index);
		
		SHORT GetCodonsNumber();

		operator SHORT() const {return index;}
		operator char() const {return letter;}
		operator std::string() const {return name;}


		~AMINOACID();


	private:
		SHORT index;
		
		char letter;
		std::string name;
				
		SHORT groupIndex;
		
		std::vector <SHORT> codonsIndices;

		DATABASE* database;
};