#pragma once
#include "fots_core.h"
#include "database.h"


class MUTATION_NUCLEOTIDE
{
	public:
		MUTATION_NUCLEOTIDE();
		MUTATION_NUCLEOTIDE(
				const MUTATION_NUCLEOTIDE& other);
		MUTATION_NUCLEOTIDE(
				const SHORT nucleotide1Index,
				const SHORT nucleotide2Index,
				const unsigned int position,
				DATABASE* database);

		MUTATION_NUCLEOTIDE& operator=(
				const MUTATION_NUCLEOTIDE& other);


		std::string GetProcess(bool forCodon = true);

		double GetDifficulty();
		
		bool MutationIsTransition();
		bool TransversionIsFromPurineToPyrimidine();

		void Show();


		~MUTATION_NUCLEOTIDE();


	private:
		SHORT nucleotide1Index;
		SHORT nucleotide2Index;

		std::string processForCodon;
		std::string processForDNA;

		unsigned int position;

		double difficulty;
		
		bool mutationIsTransition;

		DATABASE* database;
};