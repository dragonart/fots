#pragma once
#include "fots_core.h"
#include "database.h"


class NUCLEOTIDE
{
	public:
		NUCLEOTIDE();
		NUCLEOTIDE(
				const NUCLEOTIDE& other);
		NUCLEOTIDE(
				const SHORT index, 
				const char letter, 
				const std::string name,
				const bool typeIsPurine,
				const SHORT pairInDNAIndex,
				const SHORT pairINRNAIndex,
				DATABASE* database);
	

				
		bool TypeIsPurine();
		
		SHORT GetPairInDNAIndex();
		NUCLEOTIDE& GetPairInDNA();

		SHORT GetPairInRNAIndex();
		NUCLEOTIDE& GetPairInRNA();

		operator SHORT() const {return index;}
		operator char() const {return letter;}
		operator std::string() const {return name;}



		~NUCLEOTIDE();


	private:
		SHORT index;

		char letter;
		std::string name;
		
		bool typeIsPurine;
		
		SHORT pairInDNAIndex;
		SHORT pairInRNAIndex;

		DATABASE* database;
};