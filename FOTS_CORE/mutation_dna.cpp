#include "mutation_dna.h"
#include "mutation_nucleotide.h"
#include "database.h"
#include "dna.h"
#include "nucleotide.h"


MUTATION_DNA::MUTATION_DNA()
{
	dna1Index = 0;
	dna2Index = 0;

	process = std::string("");

	difficulty = 0.0;

	transitionsNumber = 0;
	transversionsNumber = 0;

	transversionsFromPurineToPyrimidineNumber = 0;

	line1 = "";
	line2 = "";

	length = 0;

	database = NULL;
}

MUTATION_DNA::MUTATION_DNA(
		const MUTATION_DNA& other)
{
	dna1Index = other.dna1Index;
	dna2Index = other.dna2Index;

	process = other.process;

	difficulty = other.difficulty;

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	transversionsFromPurineToPyrimidineNumber = other.transversionsFromPurineToPyrimidineNumber;

	line1 = other.line1;
	line2 = other.line2;

	length = other.length;

	mutations = other.mutations;

	database = other.database;
}

MUTATION_DNA::MUTATION_DNA(
		const LONG newDNA1Index, 
		const LONG newDNA2Index, 
		DATABASE* newDatabase)
{
	database = newDatabase;

	dna1Index = newDNA1Index;
	dna2Index = newDNA2Index;

	line1 = (std::string)database->dnasData[dna1Index];
	line2 = (std::string)database->dnasData[dna2Index];
	

	std::vector <std::string> lines = database->AlignLines(ALIGN_DNAS, dna1Index, dna2Index);
	line1 = lines[0];
	line2 = lines[1];

	length = line1.length();


	// ��������� ���� ����������

	process = "";
	
	difficulty = 0.0;

	transversionsNumber = transitionsNumber = transversionsFromPurineToPyrimidineNumber = 0;


	short gapStarted = 0; // 0 - ������� ���. 1 - � ������, 2 - �� ������
	for(LONG i = 0; i < length; i++)
	{
		SHORT index1 = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line1[i]);
		SHORT index2 = (SHORT)database->nucleotidesData.GetNucleotideFromLetter(line2[i]);

		// ������� ���������� ������������� ������������ ��������
		if(index1 == index2)
			continue;

		// ���� ���� �� ����������� �����, ������ ����� ����� ������
		if(!(index1 * index2))
		{
			// ���� ������ ������ ��� ��������, ����� ���� ��������
			if(!gapStarted)
			{
				process += "[ gap in DNA #";

				if(!index1)
				{
					gapStarted = 1;
					process += "1 ]\n";
				}
				else
				{
					gapStarted = 2;
					process += "2 ]\n";
				}
			}
			else
				// �������� ����, ���� ����� ������ �� � ��� �� ���
				if((gapStarted == 1) && !index2)
				{
					gapStarted = 2;
					process += "[ gap in DNA #2 ]\n";
				}
				else
					if((gapStarted == 2) && !index1)
					{
						gapStarted = 1;
						process += "[ gap in DNA #1 ]\n";
					}

			continue;
		}

		gapStarted = 0;
		mutations.push_back(MUTATION_NUCLEOTIDE(index1, index2, i, database));

		/*
			������ ������:
			��� ������� � �������
									*/
		process += mutations[mutations.size() - 1].GetProcess(false) + "\n";

		difficulty += mutations[mutations.size() - 1].GetDifficulty();

		transversionsNumber += (!mutations[mutations.size() - 1].MutationIsTransition());
		transitionsNumber += mutations[mutations.size() - 1].MutationIsTransition();

		transversionsFromPurineToPyrimidineNumber += (database->nucleotidesData[index1].TypeIsPurine() && !database->nucleotidesData[index2].TypeIsPurine());
	}
}

MUTATION_DNA& MUTATION_DNA::operator=(
		const MUTATION_DNA& other)
{
	dna1Index = other.dna1Index;
	dna2Index = other.dna2Index;

	process = other.process;

	difficulty = other.difficulty;

	transitionsNumber = other.transitionsNumber;
	transversionsNumber = other.transversionsNumber;

	transversionsFromPurineToPyrimidineNumber = other.transversionsFromPurineToPyrimidineNumber;

	mutations = other.mutations;

	line1 = other.line1;
	line2 = other.line2;

	length = other.length;

	database = other.database;

	return *this;
}




std::string MUTATION_DNA::GetProcess()
{
	return process;
}


double MUTATION_DNA::GetDifficulty()
{
	return difficulty;
}

		
int MUTATION_DNA::GetTransitionsNumber()
{
	return transitionsNumber;
}

int MUTATION_DNA::GetTransversionsNumber()
{
	return transversionsNumber;
}


int MUTATION_DNA::GetTransversionsFromPurineToPyrimidineNumber()
{
	return transversionsFromPurineToPyrimidineNumber;
}

int MUTATION_DNA::GetTransversionsFromPyrimidineToPurineNumber()
{
	return transversionsNumber - transversionsFromPurineToPyrimidineNumber;
}



int MUTATION_DNA::GetMutationsNumber()
{
	return mutations.size();
}


void  MUTATION_DNA::Show()
{
	std::cout << "DNA #1: " << database->dnasData[dna1Index].GetName() << "\n";
	std::cout << "DNA #2: " << database->dnasData[dna2Index].GetName() << "\n";
	std::cout << "LINE #1: " << line1 << "\n";
	std::cout << "LINE #2: " << line2 << "\n";
	if(dna1Index == dna2Index)
		std::cout << "THERE IS NO MUTATION\n";
	else
	{
		std::cout << "PROCESS:\n" << process << "\n";
		std::cout << "PART OF EQULAL NUCLEOTIDES: " << CalculateEqualNucleotidesPart() * 100.0 << "%\n";
		std::cout << "TOTAL TRANSITIONS: " << transitionsNumber << "\n\n";
		std::cout << "TOTAL TRANSVERSIONS: " << transversionsNumber << "\n";
		std::cout << "TRANSVERSIONS FROM PURINE TO PYRIMIDINE: " << GetTransversionsFromPurineToPyrimidineNumber() << "\n";
		std::cout << "TRANSVERSIONS FROM PYRIMIDINE TO PURINE: " << GetTransversionsFromPyrimidineToPurineNumber() << "\n\n";
		std::cout << "CHANGING OF BELOSERSKY COEFFICIENT: " << database->dnasData[dna1Index].CalculateBeloserskyCoefficient() << " -> " << database->dnasData[dna2Index].CalculateBeloserskyCoefficient() << "\n";
	}
}
		
void  MUTATION_DNA::SaveToFile(
		const std::string fileName)
{
	std::ofstream file;

	file.open(fileName);
	if(!file.is_open())
	{
		file.close();
		
		file.open(fileName, std::ios_base::out | std::ios_base::trunc);
			if(!file.is_open())
				return;
	}

	
	file << "PEPTIDE #1: " << database->dnasData[dna1Index].GetName() << "\n";
	file << "PEPTIDE #2: " << database->dnasData[dna2Index].GetName() << "\n";
	file << "LINE #1: " << line1 << "\n";
	file << "LINE #2: " << line2 << "\n";
	if(dna1Index == dna2Index)
		file << "THERE IS NO MUTATION\n";
	else
	{
		file << "PROCESS:\n" << process << "\n";
		file << "PART OF EQULAL NUCLEOTIDES: " << CalculateEqualNucleotidesPart() * 100.0 << "%\n";
		file << "TOTAL TRANSITIONS: " << transitionsNumber << "\n\n";
		file << "TOTAL TRANSVERSIONS: " << transversionsNumber << "\n";
		file << "TRANSVERSIONS FROM PURINE TO PYRIMIDINE: " << GetTransversionsFromPurineToPyrimidineNumber() << "\n";
		file << "TRANSVERSIONS FROM PYRIMIDINE TO PURINE: " << GetTransversionsFromPyrimidineToPurineNumber() << "\n\n";
		file << "CHANGING OF BELOSERSKY COEFFICIENT: " << database->dnasData[dna1Index].CalculateBeloserskyCoefficient() << " -> " << database->dnasData[dna2Index].CalculateBeloserskyCoefficient() << "\n";
	}

	file.close();
	std::cout << "Mutation successfully saved to file.\n";
}

		
double  MUTATION_DNA::CalculateEqualNucleotidesPart()
{
	return (double)(database->dnasData[dna1Index].GetLength() - GetMutationsNumber()) / (double)database->dnasData[dna1Index].GetLength();
}
		
		
bool MUTATION_DNA::operator==(
		const MUTATION_DNA& other)
{
	if(dna1Index != other.dna1Index)
		return false;
	if(dna2Index != other.dna2Index)
		return false;

	// � �������� ����������

	return true;
}


MUTATION_DNA::~MUTATION_DNA()
{
}