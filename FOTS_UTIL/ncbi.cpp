#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../FOTS_CORE/fots_core.h"


using namespace std;


int main(int argc, char** argv)
{
	ifstream input;
	ofstream out;

	out.open("new peptides.txt");

	if(!out.is_open())
	{
		cout << "Fail on create output!\n";
		return 1;
	}

	for(int i = 1; i < argc; i++)
	{
		input.open(argv[i]);

		if(!input.is_open())
		{
			cout << "Fail on open input!\n";
			return 1;
		}


		vector <string> lines;
		string line;

		// ������ ������ - ��� �������
		getline(input, line);
		lines.push_back(line);

		// ������ ��������� ����� �� �����
		while(!input.eof())
		{
			getline(input, line);

			// ���������� �������� � ������ ������ (���� ����)
			int position = line.find_first_not_of(" ");
			line = line.substr(position);

			// ����� ����� � ������ ������: ����� ���� ��� ������
			position = line.find_first_of(" ");
			line = line.substr(position);

			// ��������� ��������
			position = line.find_first_not_of(" ");
			line = line.substr(position);

			// ������ ������ ������� �� ��������� ������ ��������, ���������� ���������
			while((position = line.find_first_of(" ")) != -1)
			{
				string subline = line.substr(0, position);
				lines.push_back(subline);
				line = line.substr(position + 1);
			}
		
			lines.push_back(line);
		}

		input.close();




	
		out << lines[0] << "\n";
		
		for(int i = 1; i < lines.size(); i++)
		{
			for(int j = 0; j < lines[i].length(); j++)
				if(lines[i][j] >= 'a' && lines[i][j] <= 'z')
					out << char(lines[i][j] + 'A' - 'a');
				else
					if(lines[i][j] >= 'A' && lines[i][j] <= 'Z')
						out << lines[i][j];
					else
						out << LETTER_WRONG;
		}

		out << "\n\n";
	}

	
	out.close();
	cout << "Success!\n";
	return 0;
}